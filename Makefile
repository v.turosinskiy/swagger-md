MAKEFILE_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

include $(MAKEFILE_DIR)/.make/Makefile-help

SWAGGER_MD_BIN := $(MAKEFILE_DIR)/bin/swaggermd

.PONY: help
help: # @common Help Target asdfasdf
	@perl -e '$(HELP_FUN)' $(MAKEFILE_LIST)

.PHONY : swgmd-help
swgmd-help: ## @swagger-md Help
	$(PHP_BIN) $(SWAGGER_MD_BIN)

.PHONY : swgmd-conf
swgmd-conf: ## @swagger-md Show configuration
	$(PHP_BIN) $(SWAGGER_MD_BIN) swgmd-conf

.PHONY : swgmd-md
swgmd-md: ## @swagger-md Create markdown documentation
	$(PHP_BIN) $(SWAGGER_MD_BIN) swgmd-md

.PHONY : swgmd-scan
swgmd-scan: ## @swagger-md Create json file with OpenAPI 2 specification
	$(PHP_BIN) $(SWAGGER_MD_BIN) swgmd-scan

.PHONY : swgmd-clean
swgmd-clean: ## @swagger-md Clean markdown documentation directory
	$(PHP_BIN) $(SWAGGER_MD_BIN) swgmd-clean

.PHONY : swgmd-build
swgmd-build: ## @swagger-md Rebuild markdown documentation
	$(PHP_BIN) $(SWAGGER_MD_BIN) swgmd-build

.PHONY : clean
clean: ## @common Clean make dependencies
