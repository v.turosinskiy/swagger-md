# SwaggerMD

SwaggerMD - библиотека для генерации markdown документации на основе swagger.json файла

## Используемые компоненты
Для парсинга аннотаций используется библиотека [zircote/swagger-php](https://github.com/zircote/swagger-php) которая поддерживает стандарт [OpenAPI Specification 2](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md)

## Установка
```bash
composer require --dev https://gitlab.notamedia.ru/mos-libs/swagger-md
```

## Настройка
1. Скопировать и переименовать (например в `doc-genertor`) папку [project-template](./project-template) в корень проекта
2. Создать файл `doc-generator/config/services.local.yml` используя файл [example.services.local.yml](./project-template/config/example.services.local.yml) 
3. Настроить параметры:
  - `swagger.scan` - директории и файлы с аннотациями
  - `swagger.build_dir` - директория куда будут сохраняться md файлы
  - `swagger.swagger_json` - файл с json данными соответствующий спецификации OpenAPI 2
  - `templates.directory` - директория с шаблонами для страниц документации

## Список команд

1. Помощь
    ```bash
    make help
    ```
2. Удаление файлов в папке с Markdown документацией
    ```bash
    make swg-clean
    ``` 
3. Чтение аннотаций и создание swagger.json файла
    ```bash
    make swg-scan
    ```
4. Чтение swagger.json файла и создание Markdown документации
    ```bash
    make swg-md
    ```
5. Пересборка документации (последовательный вызов команд из пунктов 2,3,4)
    ```bash
    make swg-build
    ```
## Быстрый старт
1. [Структура проекта](./docs/quick-start/Readme.md)
2. [Общее описание сервиса](./docs/quick-start/Swagger.md)
3. [Описание моделей](./docs/quick-start/Model.md)
4. [Описание методов](./docs/quick-start/Action.md)
5. [Особенности генерации Markdown документации](./docs/quick-start/Markdown.md)
6. [Работа с сервисами страниц](./docs/quick-start/PageServices.md)
7. [Работа с расширениями](./docs/quick-start/Extensions.md)

## Расширенная документация по классам
1. [@SWG\Contact](./docs/write_annotations/Contact.md) - Контактных данных
2. [@SWG\Definition](./docs/write_annotations/Definition.md) - Константы
3. [@SWG\Delete](./docs/write_annotations/Tag.md) - Запрос Delete
4. [@SWG\ExternalDocumentation](./docs/write_annotations/ExternalDocumentation.md) - Ссылка на подробную документацию
5. [@SWG\Get](./docs/write_annotations/Get.md) - Запрос Get
6. [@SWG\Head](./docs/write_annotations/Head.md) - Запрос Head
7. [@SWG\Header](./docs/write_annotations/Header.md) - Заголовок
8. [@SWG\Info](./docs/write_annotations/Info.md) - Информация о проекте
9. [@SWG\Items](./docs/write_annotations/Items.md) - Элемент массива
10. [@SWG\License](./docs/write_annotations/License.md) - Используемой лицензии
11. [@SWG\Operation](./docs/write_annotations/Operation.md) - Операции
12. [@SWG\Options](./docs/write_annotations/Options.md) - Опции запроса
13. [@SWG\Parameter](./docs/write_annotations/Parameter.md) - Параметр запроса
14. [@SWG\Patch](./docs/write_annotations/Patch.md) - Запроса Patch
15. [@SWG\Path](./docs/write_annotations/Path.md) - Путь запроса
16. [@SWG\Post](./docs/write_annotations/Post.md) - Запрос Post
17. [@SWG\Property](./docs/write_annotations/Property.md) - Свойство
18. [@SWG\Put](./docs/write_annotations/Put.md) - Запрос Put
19. [@SWG\Response](./docs/write_annotations/Response.md) - Ответ на запрос
20. [@SWG\Schema](./docs/write_annotations/Schema.md) - Схема обьекта
21. [@SWG\SecuritySchema](./docs/write_annotations/SecuritySchema.md) - Схемы безопасности для авторизации
22. [@SWG\Swagger](./docs/write_annotations/Swagger.md) - Данные о Swagger
23. [@SWG\Tag](./docs/write_annotations/Tag.md) - Сущность
24. [@SWG\XML](./docs/write_annotations/XML.md) - XML обьект
