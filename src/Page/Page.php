<?php

namespace moslibs\SwaggerMD\Page;

use moslibs\SwaggerMD\Output\FileOutputInterface;

/**
 * Класс для хранения информации об отдельной страницы документации
 *
 * @property FileOutputInterface $fileOutput
 */
class Page implements PageInterface
{
    protected $outputFile;
    protected $content;
    protected $fileOutput;

    /**
     * @inheritdoc
     */
    public function __construct($outputFile, $content)
    {
        $this->outputFile = $outputFile;
        $this->content = $content;
    }

    /**
     * @inheritdoc
     */
    public function getOutputFile()
    {
        return $this->outputFile;
    }

    /**
     * @inheritdoc
     */
    public function getContent()
    {
        return $this->content;
    }
}