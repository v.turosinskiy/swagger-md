<?php

namespace moslibs\SwaggerMD\Page;

/**
 * Интерфейс для хранения информации об отдельной страницы документации
 */
interface PageInterface
{
    /**
     * Возвращает путь к файлу для сохранения страницы
     *
     * @return string
     */
    public function getOutputFile();

    /**
     * Возвращает массив данных для передачи в шаблон
     *
     * @return array
     */
    public function getContent();
}