<?php

namespace moslibs\SwaggerMD\Exception;

use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Ошибка при попытке загрузить файл конфигураций
 */
class ConfigNotFoundException extends Exception
{
    /**
     * Конструктор
     *
     * @param string $configPath - путь до файла с конфигурацией
     * @param int    $code       - код ответа
     */
    public function __construct($configPath, $code = 500)
    {
        $message = 'Configuration file ' . $configPath . ' is not found';
        parent::__construct($message, $code);
    }
}
