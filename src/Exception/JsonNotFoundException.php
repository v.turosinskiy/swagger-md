<?php

namespace moslibs\SwaggerMD\Exception;

/**
 * Ошибка при попытке загрузить файл swagger.json
 */
class JsonNotFoundException extends \Exception
{
    /**
     * Конструктор
     *
     * @param string $file - путь до файла
     * @param int    $code - код ответа
     */
    public function __construct($file, $code = 500)
    {
        $message = 'Json ' . $file . ' not found';
        parent::__construct($message, $code);
    }
}
