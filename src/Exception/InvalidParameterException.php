<?php

namespace moslibs\SwaggerMD\Exception;

/**
 * Ошибка при попытке загрузить значение параметра
 */
class InvalidParameterException extends \Exception
{
    /**
     * Конструктор
     *
     * @param string $parameter - ключ параметра
     * @param int    $code      - код ответа
     */
    public function __construct($parameter, $code = 500)
    {
        $message = 'Parameter ' . $parameter . ' is invalid';
        parent::__construct($message, $code);
    }
}
