<?php

namespace moslibs\SwaggerMD\Exception;

use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Ошибка при попытке найти файл с автозагрузчиком
 */
class AutoloaderNotFoundException extends Exception
{
    /**
     * Конструктор
     *
     * @param array $files - Список файлов, в которых производился поиск
     * @param int   $code  - код ответа
     */
    public function __construct($files, $code = 500)
    {
        $message = 'Autoload file is not found. Checked files: ' . implode(', ', $files);
        parent::__construct($message, $code);
    }
}
