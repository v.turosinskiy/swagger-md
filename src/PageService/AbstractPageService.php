<?php

namespace moslibs\SwaggerMD\PageService;

use moslibs\SwaggerMD\Config\Config;
use moslibs\SwaggerMD\PageService\Extension\ExtensionInterface;
use moslibs\SwaggerMD\PageService\Traits\MarkdownTrait;

/**
 * Сервис страниц предназначен для создания массива из обьектов страниц - Page
 */
abstract class AbstractPageService implements PageServiceInterface
{
    use MarkdownTrait;

    /**
     * Путь к файлу с шаблоном для рендеринга отдельной страницы
     *
     * @var string
     */
    protected $templateName;

    /**
     * Настройки сервиса
     *
     * @var Config
     */
    protected $config;

    /**
     * количество страниц, которое сервис может создать
     *
     * @var int
     */
    protected $numberOfPages = 0;

    /**
     * Массив зарегистрированных расширений
     *
     * @var ExtensionInterface[]
     */
    protected $extensions = [];

    /**
     * Колонки для таблицы параметров
     *
     * @var array
     */
    protected $mainParameterFields = [
        'name',
        'short_name',
        'in',
        'type'
    ];

    /**
     * Колонки для таблицы с модели
     *
     * @var array
     */
    protected $mainPropertyFields = [
        'name',
        'short_name',
        'type'
    ];

    /**
     * Дополнительные свойства для свойств и параметров
     *
     * @var array
     */
    protected $additionalFields = [
        'default',
        'full_description',
        'enum',
        'optional_value',
        'minLength',
        'maxLength',
        'array_enum'
    ];

    /**
     * Заголовки используемые при генерации таблиц
     *
     * @var array
     */
    protected $titles = [
        'name'       => 'Ключ',
        'in'         => 'Передается в',
        'short_name' => 'Название',
        'type'       => 'Тип',
        'filterable' => 'Фильтрация',
        'sortable'   => 'Сортировка',
        'optional'   => 'По умолчанию',
    ];

    /**
     * Конструктор
     *
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
        $this->prepare();
    }

    /**
     * Подготовка сервиса перед инициализацией
     * Используется например для подсчета кол-ва страниц
     * которое будет возвращено сервисом
     */
    protected function prepare()
    {

    }

    /**
     * @inheritdoc
     */
    public function getNumberOfPages()
    {
        return $this->numberOfPages;
    }

    /**
     * Добавляет расширение
     *
     * @param ExtensionInterface $extension
     */
    public function registerExtension(ExtensionInterface $extension)
    {
        $key = get_class($extension);
        if (!in_array($key, $this->extensions)) {
            $this->extensions[$key] = $extension;
        }
    }

    /**
     *
     *
     * @param array $context
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function render($context)
    {
        $template = $this->getTemplateName();
        return $this->config->getTwig()->load($template)->render($context);
    }

    /**
     * Возвращает название шаблона используемый сервисом
     * для создания страниц
     */
    public function getTemplateName()
    {
        return $this->templateName;
    }

    /**
     * Указание шаблона, который сервис должен использовать для создания страниц
     *
     * @param string $templateName
     */
    public function setTemplateName($templateName)
    {
        $this->templateName = $templateName;
    }

    public function createPage($file, $content)
    {
        return $this->config->getPageFactory()->createPage($file, $content);
    }

    /**
     * Преобразует данные в $context с помощью расширений перед передачей в шаблон
     *
     * @param array $context
     * @return array
     */
    protected function updateContextWithExtensions($context)
    {
        $extensions = $this->getExtensions();
        if (!empty($extensions)) {
            foreach ($extensions as $extension) {
                $context = $extension->update($context);
            }
        }
        return $context;
    }

    /**
     * Возвращает список подключенных расширений
     *
     * @return ExtensionInterface[]
     */
    public function getExtensions()
    {
        return $this->extensions;
    }
}