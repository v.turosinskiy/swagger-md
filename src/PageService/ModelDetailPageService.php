<?php

namespace moslibs\SwaggerMD\PageService;

/**
 * Класс для генерации детальной страницы модели
 */
class ModelDetailPageService extends AbstractPageService
{
    /**
     * @inheritdoc
     */
    public function getPages()
    {
        $pages = [];
        $swagger = $this->config->getSwagger();

        if (!empty($swagger->definitions)) {
            foreach ($swagger->definitions as $name => $definition) {
                if (!empty($definition['xml']['name'])) {
                    $context = $this->getContext($name, $definition);
                    $content = $this->render($context);
                    $pages[] = $this->createPage($context['outputFile'], $content);
                }
            }
        }

        return $pages;
    }

    /**
     * Подготовка данных для передачи в шаблон
     *
     * @param $name
     * @param $definition
     * @return array
     * @throws \Exception
     */
    public function getContext($name, $definition)
    {
        $schemaHelper = $this->config->getSchemaHelper();
        $outputFile = $schemaHelper->getDocPathByDefinitionTitle($name);
        $definition = $schemaHelper->prepareDefinition($definition);
        $context = [
            'outputFile'       => $outputFile,
            'rootDir'          => '..' . DIRECTORY_SEPARATOR,
            'definition'       => $definition,
            'name'             => $name,
            'mainFields'       => $this->mainPropertyFields,
            'additionalFields' => $this->additionalFields,
            'headers'          => $this->titles,
        ];

        //Передаем другим расширениям, чтобы они могли модифицировать данные
        $context = $this->updateContextWithExtensions($context);

        if (!empty($context['definition']['properties'])) {
            $context = $this->renderDefinition($context);
        }
        return $context;
    }

    /**
     * Подготовка данных о свойствах модели перед передачей в шаблон
     *
     * @param array $context
     * @return array
     * @throws \Exception
     */
    protected function renderDefinition($context)
    {
        $keys = $headers = [];
        if (!empty($context['definition']['properties'])) {
            foreach ($context['mainFields'] as $fieldKey) {
                if (!empty($context['headers'][$fieldKey])) {
                    $headers[$fieldKey] = $context['headers'][$fieldKey];
                }
            }

            $tableProperties = $context['definition']['properties'];
            $properties = [];
            foreach ($tableProperties as $name => $property) {
                if (!empty(!in_array($name, $keys))) {
                    $keys[] = $name;
                    if (array_intersect(array_keys($property), $context['additionalFields'])) {
                        $properties[] = $property;
                    }
                    if (!empty($property['readmeFile'])) {
                        $tableProperties[$name]['short_name'] = [
                            'value' => '[' . $property['short_name'] . ']',
                            'link'  => '..' . DIRECTORY_SEPARATOR . '..' . $property['readmeFile']
                        ];
                    }
                }
            }

            $table = $this->config->getTableHelper()->renderItemsAsTable($tableProperties, $headers);
            $context['table'] = $table;
            $context['additionalDescription'] = $properties;
            $context['example'] = $this->config->getSchemaHelper()->getExampleByModel($context['definition']);
        }
        return $context;
    }

    /**
     * Переопределяем метод для подсчета количества страниц
     */
    protected function prepare()
    {
        $swagger = $this->config->getSwagger();
        if (!empty($swagger->definitions)) {
            foreach ($swagger->definitions as $definition) {
                if (!empty($definition['xml']['name'])) {
                    $this->numberOfPages++;
                }
            }
        }
    }
}