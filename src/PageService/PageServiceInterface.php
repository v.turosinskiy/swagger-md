<?php

namespace moslibs\SwaggerMD\PageService;

use moslibs\SwaggerMD\Page\PageInterface;

/**
 * Интерфейс для генерации страниц документации
 */
interface PageServiceInterface
{
    /**
     * Возвращает список страниц
     *
     * @return PageInterface[]
     */
    public function getPages();

    /**
     * Возвращает страницу в виде строки
     *
     * @param array $content - данные для шаблона
     * @return string
     */
    public function render($content);

    /**
     * Возвращает количество страниц которое будет создано
     *
     * @return integer
     */
    public function getNumberOfPages();
}