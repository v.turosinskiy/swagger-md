<?php

namespace moslibs\SwaggerMD\PageService\Extension;

use moslibs\SwaggerMD\Helper\Schema;

/**
 * На основе схемы ответа формирует пример в виде массива в формате ключ и значение
 */
class ExampleResponseExtension implements ExtensionInterface
{
    /**
     * Помощник для работы с OpenAPI спецификацией
     *
     * @var Schema
     */
    protected $schemaHelper;

    public function __construct(Schema $schemaHelper)
    {
        $this->schemaHelper = $schemaHelper;
    }

    /**
     * @inheritdoc
     */
    public function update($context)
    {
        if (!empty($context['action']['responses'])) {
            foreach ($context['action']['responses'] as $code => $response) {
                if (!empty($response['schema'])) {
                    $model = $this->schemaHelper->getModelBySchema($response['schema']);
                    $example = $this->schemaHelper->getExampleBySchema($model);
                    $context['responses']['data'][$code]['example'] = $example;
                }
            }
        }

        return $context;
    }
}