<?php

namespace moslibs\SwaggerMD\PageService\Extension;

use moslibs\SwaggerMD\Helper\Schema;


/**
 * Отображает список свойств, возвращаемые только через expand поле
 */
class ExpandParameterExtension implements ExtensionInterface
{
    /**
     * Помощник для работы с OpenAPI спецификацией
     *
     * @var Schema
     */
    protected $schemaHelper;

    public function __construct(Schema $schemaHelper)
    {
        $this->schemaHelper = $schemaHelper;
    }

    /**
     * @inheritdoc
     */
    public function update($context)
    {
        $success = 200;
        if (empty($context['action']['parameters'])) {
            return $context;
        }

        if($context['path'] == '/api/newsfeed/v4/backend/json/{lang}/press/interview'){
            $test = 1;;
        }

        $enumValues = [];
        foreach ($context['action']['parameters'] as $parameter) {
            if (!empty($parameter['name']) && $parameter['name'] == 'expand' && !empty($parameter['items']['enum'])) {
                $enumValues = $parameter['items']['enum'];
            }
        }

        if (!empty($context['action']['responses'][$success]['properties'])) {
            $propKeys = array_keys($context['action']['responses'][$success]['properties']);
        }

        if (!empty($enumValues) && !empty($propKeys)) {

            $items = [];
            foreach ($enumValues as $name) {
                if (!in_array($name, $propKeys)) {
                    if (!in_array('optional', $context['responses']['mainFields'])) {
                        $context['responses']['mainFields'][] = 'optional';
                    }
                    $refKey = (!empty($context['action']['expand-ref'][$name])) ? $context['action']['expand-ref'][$name] : $name;
                    $ref = '#/definitions/CommonProperties/properties/' . $refKey;
                    $item = ['$ref' => $ref, 'name' => $name];
                    $refItem = $this->schemaHelper->getPropertyByRef($item);
                    $refItem['optional'] = 'expand';
                    if (!empty($refItem)) {
                        $items[$name] = $refItem;
                    }
                }
            }

            $properties = $this->schemaHelper->prepareProperties(['properties' => $items]);
            $originalProperties = $context['action']['responses'][$success]['properties'];
            $context['action']['responses'][$success]['properties'] = array_merge($originalProperties, $properties);
        }
        return $context;
    }
}