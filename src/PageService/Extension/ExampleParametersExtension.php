<?php

namespace moslibs\SwaggerMD\PageService\Extension;


/**
 * Отображает примеры для параметров с помощью отдельного файла
 * В файле должен соержаться ассоциативный массив вида:
 * [
 *  "example_some_property": {
 *      "id":1,
 *      "title":"Some Property"
 *  }
 * ]
 */
class ExampleParametersExtension implements ExtensionInterface
{
    /**
     * Массив с примерами схем
     *
     * @var array
     */
    protected $json = [];

    public function __construct($json = [])
    {
        $this->json = $json;
    }

    /**
     * Добавляет файл с примерами
     *
     * @param string $file - путь к файлу с ассациативным массивом
     */
    public function withJsonFromFile($file)
    {
        if (file_exists($file)) {
            $content = file_get_contents($file);
            $this->json = json_decode($content, true);
        }
    }

    /**
     * @inheritdoc
     */
    public function update($context)
    {
        $json = $this->json;
        $context['parameters']['additionalFields'][] = 'formExample';
        if (!empty($json) && !empty($context['action']['parameters'])) {
            foreach ($context['action']['parameters'] as &$parameter) {
                if (!empty($parameter['x-formExample']) && !empty($json[$parameter['x-formExample']])) {
                    $parameter['formExample'] = [$parameter['name'] => $json[$parameter['x-formExample']]];
                }
            }
            unset($parameter);
        }
        return $context;
    }
}