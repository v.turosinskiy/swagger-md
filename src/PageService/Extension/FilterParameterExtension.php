<?php

namespace moslibs\SwaggerMD\PageService\Extension;


/**
 * Если передается параметр filter, то добавляет в таблицу "Возвращаемые поля"
 * новую колонку "Фильтруемое"
 */
class FilterParameterExtension implements ExtensionInterface
{
    /**
     * @inheritdoc
     */
    public function update($context)
    {
        if (empty($context['action']['parameters'])) {
            return $context;
        }
        foreach ($context['action']['parameters'] as $parameter) {
            if (!empty($parameter['name']) && $parameter['name'] == 'filter') {
                $context['responses']['mainFields'][] = 'filterable';
            }
        }

        if (!empty($context['responses']['mainFields']) && in_array('filterable', $context['responses']['mainFields'])) {
            $values = (!empty($context['action']['x-filter'])) ? $context['action']['x-filter'] : [];
            foreach ($context['action']['responses'] as &$response) {
                if (!empty($response['properties'])) {
                    foreach ($response['properties'] as &$property) {
                        if (!empty($values)) {
                            $filterable = (in_array($property['name'], $values)) ? true : false;
                        } else {
                            $filterable = (!empty($property['x-filterable']) && $property['x-filterable'] === false) ? false : true;
                        }

                        $property['filterable'] = $filterable;
                    }
                    unset($property);
                }
            }
            unset($response);
        }

        return $context;
    }
}