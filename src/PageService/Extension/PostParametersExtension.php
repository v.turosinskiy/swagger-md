<?php

namespace moslibs\SwaggerMD\PageService\Extension;

use moslibs\SwaggerMD\Helper\Schema;

/**
 * Метод подставляет свойства сохраняемой модели в список параметров с учетом полей require и readOnly
 *
 * @property Schema $schemaHelper
 */
class PostParametersExtension implements ExtensionInterface
{
    /**
     * Помощник для работы с OpenAPI спецификацией
     *
     * @var Schema
     */
    protected $schemaHelper;

    public function __construct(Schema $schemaHelper)
    {
        $this->schemaHelper = $schemaHelper;
    }

    /**
     * @inheritdoc
     */
    public function update($context)
    {
        $schemaHelper = $this->schemaHelper;
        if (!empty($context['action']['x-action'])) {
            $keys = [];
            $action = $context['action'];
            $actionData = explode('/', $action['x-action']);
            $definition = $actionData[1];
            $actionType = $actionData[0];

            foreach ($action['parameters'] as $key => $parameter) {
                $keys[] = $parameter['name'];
            }

            if (!empty($definition) && !empty($actionType)) {
                $ref = '#/definitions/' . $definition;
                $model = ['$ref' => $ref];
                $refItem = $schemaHelper->getPropertyByRef($model);
                foreach ($refItem['properties'] as $key => $property) {
                    if (!in_array($key, $keys)) {
                        $refProperty = $schemaHelper->getPropertyByRef($property);
                        if (!empty($refProperty['description'])) {
                            if (!empty($refProperty['readOnly']) && $refProperty['readOnly'] === true) {
                                continue;
                            }

                            $parameter = $refProperty;
                            $parameter['name'] = $key;
                            $parameter['in'] = 'body';

                            $action['parameters'][] = $parameter;
                        }
                    }
                }
                $context['action']['parameters'] = $action['parameters'];
            }
        }
        return $context;
    }
}