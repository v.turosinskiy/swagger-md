<?php

namespace moslibs\SwaggerMD\PageService\Extension;

/**
 * Интерфейс для расширений
 */
interface ExtensionInterface
{
    /**
     * Метод возвращает данные которые будут переданы в шаблон
     * или следующему расширению для модификации данных
     *
     * @param $context
     * @return array
     */
    public function update($context);
}