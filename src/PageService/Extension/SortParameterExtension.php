<?php

namespace moslibs\SwaggerMD\PageService\Extension;


/**
 * Если передается параметр sort, то добавляет в таблицу "Возвращаемые поля"
 * новую колонку "Сортируемое"
 */
class SortParameterExtension implements ExtensionInterface
{
    /**
     * @inheritdoc
     */
    public function update($context)
    {
        if (empty($context['action']['parameters'])) {
            return $context;
        }
        foreach ($context['action']['parameters'] as $parameter) {
            if (!empty($context['responses']['mainFields']) && !in_array('sortable',
                    $context['responses']['mainFields']) && !empty($parameter['name']) && $parameter['name'] == 'sort') {
                $context['responses']['mainFields'][] = 'sortable';
            }
        }

        if (in_array('sortable', $context['responses']['mainFields'])) {
            $values = (!empty($context['action']['x-sort'])) ? $context['action']['x-sort'] : [];
            foreach ($context['action']['responses'] as &$response) {
                if (!empty($response['properties'])) {
                    foreach ($response['properties'] as &$property) {
                        if (!empty($values)) {
                            $sortable = (in_array($property['name'], $values)) ? true : false;
                        } else {
                            $sortable = (!empty($property['x-sortable']) && $property['x-sortable'] === false) ? false : true;
                        }

                        $property['sortable'] = $sortable;
                    }
                    unset($property);
                }
            }
            unset($response);
        }


        return $context;
    }
}