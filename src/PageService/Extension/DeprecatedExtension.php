<?php

namespace moslibs\SwaggerMD\PageService\Extension;

/**
 * Отображает список свойств, возвращаемые только через expand поле
 */
class DeprecatedExtension implements ExtensionInterface
{
    const DEPRECATED = ' (Deprecated)';

    /**
     * @inheritdoc
     */
    public function update($context)
    {
        $this->updateActions($context);
        $this->updateParameters($context);
        $this->updateDefinitions($context);
        $this->updateResponses($context);

        return $context;
    }

    protected function updateActions(&$context){
        if(!empty($context['paths'])){
            foreach ($context['paths'] as &$actionsList) {
                foreach ($actionsList as &$action) {
                    if (!empty($action['tags']) && !empty($action['summary']) && !empty($action['deprecated']) && $action['deprecated'] === true) {
                        $action['summary'] .= self::DEPRECATED;
                    }
                }
                unset($action);
            }
            unset($actionsList);
        }
    }

    protected function updateParameters(&$context){
        if (!empty($context['action']['parameters'])) {
            foreach ($context['action']['parameters'] as &$parameter) {
                if (!empty($parameter['x-deprecated']) && !empty($parameter['short_name'])) {
                    $parameter['short_name'] .= self::DEPRECATED;
                }
            }
            unset($parameter);
        }
    }

    protected function updateResponses(&$context){
        if (!empty($context['action']['responses'])) {
            foreach ($context['action']['responses'] as &$response) {
                if (!empty($response['properties'])) {
                    foreach ($response['properties'] as &$property) {
                        if (!empty($property['short_name']) && !empty($property['x-deprecated'])) {
                            $property['short_name'] .= self::DEPRECATED;
                        }
                    }
                    unset($property);
                }
            }
            unset($response);
        }
    }

    protected function updateDefinitions(&$context){
        if (!empty($context['definitions'])) {
            foreach ($context['definitions'] as &$definition) {
                if (!empty($definition['x-deprecated']) && !empty($definition['title'])) {
                    $definition['title'] .= self::DEPRECATED;
                }
            }
            unset($definition);
        }

        if (!empty($context['definition'])) {
            if (!empty($context['definition']['x-deprecated']) && !empty($context['definition']['title'])) {
                $context['definition']['title'] .= self::DEPRECATED;
            }
        }
    }
}
