<?php

namespace moslibs\SwaggerMD\PageService\Extension;


/**
 * Добавляет пометку, что поле возвращается только в определенных случаях
 */
class OptionalFieldExtension implements ExtensionInterface
{
    /**
     * @inheritdoc
     */
    public function update($context)
    {
        $addColumnFlag = false;
        foreach ($context['action']['responses'] as $code => $response) {
            if (!empty($response['schema']['x-optional'])) {
                foreach ($response['schema']['x-optional'] as $key => $value) {
                    if (!empty($response['properties'][$key])) {
                        $addColumnFlag = true;
                        $context['action']['responses'][$code]['properties'][$key]['optional'] = true;
                        $context['action']['responses'][$code]['properties'][$key]['optional_value'] = $value;
                    }
                }
            }
        }

        if ($addColumnFlag) {
            $context['action']['mainFields'][] = 'optional';
        }
        return $context;
    }
}