<?php

namespace moslibs\SwaggerMD\PageService\Traits;

/**
 * Класс для генерации страницы со списком методов
 */
trait MarkdownTrait
{
    /**
     * Форматирует путь для вызов метода по формату OpenAPI
     * в путь для сохранения md файла
     *
     * @param $path string - исходный путь
     * @return string - преобразованный путь
     */
    protected function formatPath($path)
    {
        $path = str_replace(['{', '}', '\\'], ['', '', DIRECTORY_SEPARATOR], $path);
        return $path;
    }

    /**
     * Возвращает относительный путь к корню
     *
     * @param $path
     * @return string
     */
    protected function revertPath($path)
    {
        return '..' . preg_replace("/([^\/]+)/", "..", dirname(dirname($path)));
    }
}