<?php

namespace moslibs\SwaggerMD\PageService;

/**
 * Класс для генерации детальной страницы метода
 */
class ActionDetailPageService extends AbstractPageService
{
    /**
     * @inheritdoc
     */
    public function getPages()
    {
        $pages = [];
        $swagger = $this->config->getSwagger();

        foreach ($swagger->paths as $path => $actionsList) {
            foreach ($actionsList as $method => $action) {
                $context = $this->getContext($action, $path, $method);
                $content = $this->render($context);
                $pages[] = $this->createPage($context['outputFile'], $content);
            }
        }
        return $pages;
    }

    /**
     * Подготовка данных для передачи в шаблон
     *
     * @param array  $action - данные о методе в формате OpenAPI 2
     * @param string $path   - путь вызова метода
     * @param string $method - тип метода 'get', 'post' ...
     * @return array - данные для передачи в шаблон
     */
    protected function getContext($action, $path, $method)
    {
        $action = $this->prepareAction($action);
        $outputFile = $this->getPathToMDFile($path, $method);
        $rootDir = $this->revertPath($outputFile);

        $context = [
            'method'     => $method,
            'action'     => $action,
            'path'       => $path,
            'outputFile' => $outputFile,
            'rootDir'    => $rootDir,
            'parameters' => [
                'mainFields'       => $this->mainParameterFields,
                'additionalFields' => $this->additionalFields,
                'headers'          => $this->titles
            ],
            'responses'  => [
                'mainFields'       => $this->mainPropertyFields,
                'additionalFields' => $this->additionalFields,
                'headers'          => $this->titles,
                'data'             => []
            ]
        ];

        $context = $this->updateContextWithExtensions($context);

        if (!empty($context['action']['parameters'])) {
            $context['parameters'] = $this->renderParameters($context);
        }

        if (!empty($context['action']['responses'])) {
            $context['responses']['data'] = $this->renderResponses($context);
        }
        return $context;
    }

    /**
     * Подготовка данных перед передачей в шаблон и расширения
     *
     * @param array $action
     * @return array
     */
    protected function prepareAction($action)
    {
        $schemaHelper = $this->config->getSchemaHelper();
        if (!empty($action['parameters'])) {
            $action['parameters'] = $schemaHelper->prepareParameters($action['parameters']);
        }

        if (!empty($action['responses'])) {
            $action['responses'] = $schemaHelper->prepareResponses($action['responses']);
        }

        return $action;
    }

    /**
     * Возвращает путь для сохраняемого файла на основе данных о методе
     *
     * @param string $actionPath
     * @param string $method
     * @return string
     */
    protected function getPathToMDFile($actionPath, $method)
    {
        return $this->formatPath(DIRECTORY_SEPARATOR . 'actions' . $actionPath . DIRECTORY_SEPARATOR . strtoupper($method) . '.md');
    }

    /**
     * Подготовка информации о передаваемых параметрах
     *
     * @param array $context
     * @return array
     */
    protected function renderParameters($context)
    {
        $result = $headers = $keys = $requiredParameters = $optionalParameters = [];
        $parameters = $context['action']['parameters'];
        $mainFields = $context['parameters']['mainFields'];
        $additionalFields = $context['parameters']['additionalFields'];
        $tableHelper = $this->config->getTableHelper();

        foreach ($mainFields as $fieldKey) {
            if (!empty($context['parameters']['headers'][$fieldKey])) {
                $headers[$fieldKey] = $context['parameters']['headers'][$fieldKey];
            }
        }

        foreach ($parameters as $parameter) {
            if (!empty($parameter['name'])) {
                if (!in_array($parameter['name'], $keys)) {
                    if (!empty($parameter['required']) && $parameter['required'] === true) {
                        $requiredParameters[] = $parameter;
                    } else {
                        $optionalParameters[] = $parameter;
                    }

                    $keys[] = $parameter['name'];
                }
            }
        }

        if (!empty($requiredParameters)) {
            $tableParameters = $requiredParameters;
            $parameters = [];
            foreach ($tableParameters as $key => $parameter) {
                $hasExtraFields = !empty(array_intersect(array_keys($parameter), $additionalFields));
                if ($hasExtraFields) {
                    $parameters[] = $parameter;
                }
                if (!empty($parameter['readmeFile'])) {
                    $parameter['short_name'] = [
                        'value' => '[' . $parameter['short_name'] . ']',
                        'link'  => $context['rootDir'] . $parameter['readmeFile']
                    ];
                } elseif ($hasExtraFields) {
                    $parameter['short_name'] = [
                        'value' => '[' . $parameter['short_name'] . ']',
                        'link'  => '#' . $parameter['name']
                    ];
                }
            }
            $table = $tableHelper->renderItemsAsTable($tableParameters, $headers);
            $result['required'] = [
                'table'                 => $table,
                'additionalDescription' => $parameters,
            ];
        }

        if (!empty($optionalParameters)) {
            $tableParameters = $optionalParameters;
            $parameters = [];
            foreach ($tableParameters as $key => $parameter) {
                $hasExtraFields = !empty(array_intersect(array_keys($parameter), $additionalFields));
                if ($hasExtraFields) {
                    $parameters[] = $parameter;
                }
                if (!empty($parameter['readmeFile'])) {
                    $parameter['short_name'] = [
                        'value' => '[' . $parameter['short_name'] . ']',
                        'link'  => $context['rootDir'] . $parameter['readmeFile']
                    ];
                } elseif ($hasExtraFields) {
                    $parameter['short_name'] = [
                        'value' => '[' . $parameter['short_name'] . ']',
                        'link'  => '#' . $parameter['name']
                    ];
                }

                $tableParameters[$key] = $parameter;
            }
            $table = $tableHelper->renderItemsAsTable($tableParameters, $headers);
            $result['optional'] = [
                'table'                 => $table,
                'additionalDescription' => $parameters,
            ];
        }

        return $result;
    }

    /**
     * Подготовка информации о возможных ответах на запрос
     *
     * @param array $context
     * @return array
     */
    protected function renderResponses($context)
    {

        $result = $context['responses']['data'];
        $headers = $keys = [];
        $mainFields = $context['responses']['mainFields'];
        $additionalFields = $context['responses']['additionalFields'];
        $tableHelper = $this->config->getTableHelper();
        foreach ($mainFields as $fieldKey) {
            if (!empty($context['responses']['headers'][$fieldKey])) {
                $headers[$fieldKey] = $context['responses']['headers'][$fieldKey];
            }
        }

        foreach ($context['action']['responses'] as $code => $response) {
            if (!empty($response['properties'])) {
                $tableProperties = $response['properties'];
                $properties = [];
                foreach ($tableProperties as $key => $property) {
                    if (!empty($property['name']) && !in_array($property['name'], $keys)) {
                        $keys[] = $property['name'];
                        $hasExtraFields = array_intersect(array_keys($property), $additionalFields);
                        if ($hasExtraFields) {
                            $properties[] = $property;
                        }

                        if (!empty($property['readmeFile'])) {
                            $property['short_name'] = [
                                'value' => '[' . $property['short_name'] . ']',
                                'link'  => $context['rootDir'] . $property['readmeFile']
                            ];
                        } elseif (!empty($hasExtraFields)) {
                            $property['short_name'] = [
                                'value' => '[' . $property['short_name'] . ']',
                                'link'  => '#' . $key
                            ];
                        }

                        $tableProperties[$key] = $property;
                    }
                }

                $table = $tableHelper->renderItemsAsTable($tableProperties, $headers);
                $result[$code]['table'] = $table;
                $result[$code]['additionalDescription'] = $properties;
            }
        }

        return $result;
    }

    /**
     * Переопределяем метод для подсчета количества страниц.
     * Считаем количество методов.
     */
    public function prepare()
    {
        foreach ($this->config->getSwagger()->paths as $path => $actionsList) {
            foreach ($actionsList as $method => $action) {
                $this->numberOfPages++;
            }
        }
    }
}