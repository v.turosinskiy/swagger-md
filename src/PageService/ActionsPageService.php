<?php

namespace moslibs\SwaggerMD\PageService;

/**
 * Класс для генерации страницы со списком методов
 */
class ActionsPageService extends AbstractPageService
{
    /**
     * @inheritdoc
     */
    protected $numberOfPages = 1;

    /**
     * Заголовки для таблицы со списком методов
     *
     * @var array
     */
    protected $columns = [
        'summary' => 'Название метода',
        'method'  => 'Тип метода',
        'path'    => 'Путь',
    ];

    /**
     * @inheritdoc
     */
    public function getPages()
    {
        $context = $this->getContext();
        $content = $this->render($context);
        $pages[] = $this->createPage($context['outputFile'], $content);
        return $pages;
    }

    /**
     * Подготовка данных для передачи в шаблон
     *
     * @return array
     */
    protected function getContext()
    {
        $swagger = $this->config->getSwagger();
        $outputFile = $this->getOutputFile();

        $context = [
            'paths' => $swagger->paths,
            'outputFile' => $outputFile,
            'tags' => []
        ];

        //Передаем другим расширениям, чтобы они могли модифицировать данные
        $context = $this->updateContextWithExtensions($context);

        if(!empty($context['paths'])){
            $context = $this->renderActionsList($context);
        }
        return $context;
    }

    protected function renderActionsList($context){
        $tags = [];
        foreach ($context['paths'] as $actionPath => $actionsList) {
            foreach ($actionsList as $method => $action) {
                if (!empty($action['tags']) && !empty($action['summary'])) {
                    $summary = '[' . $action['summary'] . ']';
                    foreach ($action['tags'] as $tag) {
                        $tags[$tag][] = [
                            'summary' => [
                                'value' => $summary,
                                'link'  => $this->getPathToAction($actionPath, $method)
                            ],
                            'method'  => strtoupper($method),
                            'path'    => $actionPath
                        ];
                    }
                }
            }
        }

        foreach ($tags as $tag => $rows) {
            if (!empty($rows)) {
                $t = $this->config->getTableHelper();
                $context['tags'][$tag] = [
                    'table' => $t->renderTable($this->columns, $rows)
                ];
            }
        }

        return $context;
    }

    /**
     * Возвращает относительный путь до метода
     *
     * @param $actionPath
     * @param $method
     * @return string
     */
    protected function getPathToAction($actionPath, $method)
    {
        return $this->formatPath('../actions' . $actionPath . '/' . strtoupper($method) . '.md');
    }

    /**
     * Возвращает путь д сохраняемого файла
     *
     * @return string
     */
    protected function getOutputFile()
    {
        return DIRECTORY_SEPARATOR . 'search' . DIRECTORY_SEPARATOR . 'README.md';
    }
}