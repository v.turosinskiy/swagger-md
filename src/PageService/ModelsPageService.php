<?php

namespace moslibs\SwaggerMD\PageService;

/**
 * Класс для генерации страницы со списком моделей
 */
class ModelsPageService extends AbstractPageService
{
    /**
     * @inheritdoc
     */
    protected $numberOfPages = 1;

    /**
     * @inheritdoc
     */
    public function getPages()
    {
        $pages = [];
        $context = $this->getContext();
        if (!empty($context)) {
            $content = $this->render($context);
            $pages[] = $this->createPage($context['outputFile'], $content);
        }
        return $pages;
    }

    /**
     * Подготовка данных для передачи в шаблон
     *
     * @return array
     * @throws \Exception
     */
    public function getContext()
    {
        $swagger = $this->config->getSwagger();
        $definitions = $this->prepareDefinitions($swagger->definitions);
        $outputFile = $this->getOutputFile();
        $context = [
            'outputFile' => $outputFile,
            'definitions' => $definitions
        ];

        //Передаем другим расширениям, чтобы они могли модифицировать данные
        $context = $this->updateContextWithExtensions($context);

        if (!empty($context['definitions'])) {
            $others = [];
            foreach ($context['definitions'] as $key => $definition) {
                if (empty($definition['title'])) {
                    continue;
                }
                $title = $definition['title'];

                $element = [
                    'readmeFile' => '..' . $definition['readmeFile'],
                    'title'      => $title
                ];

                if (!empty($definition['x-namespace'])) {
                    $namespaces = $definition['x-namespace'];
                    $namespaces = explode('/', $namespaces);
                    $currentData = &$context['models'];
                    foreach ($namespaces as $namespace) {
                        if (empty($currentData[$namespace])) {
                            $currentData[$namespace] = [];
                        }
                        $currentData = &$currentData[$namespace];
                    }
                    if (empty($currentData['models'])) {
                        $currentData['models'] = [];

                    }
                    $currentData['models'][] = $element;
                } else {
                    $others[] = $element;
                }
            }

            if (!empty($others)) {
                $namespace = 'Прочее';
                $context['models'][$namespace]['models'] = $others;
            }
        }
        return $context;
    }

    /**
     * Путь до сохраняемого файла
     *
     * @return string
     */
    protected function getOutputFile()
    {
        return $this->formatPath(DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'README.md');
    }

    /**
     * Подготовка моделей перед передачей в шаблон
     *
     * @param array $definitions
     * @return mixed
     * @throws \Exception
     */
    protected function prepareDefinitions($definitions)
    {
        if(!empty($definitions)){
            $schemaHelper = $this->config->getSchemaHelper();
            foreach ($definitions as $name => $definition) {
                $definitions[$name] = $schemaHelper->prepareDefinition($definition);
                $definitions[$name]['readmeFile'] = $schemaHelper->getDocPathByDefinitionTitle($name);
            }
        }

        return $definitions;
    }
}