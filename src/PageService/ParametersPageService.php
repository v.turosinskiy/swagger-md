<?php

namespace moslibs\SwaggerMD\PageService;

/**
 * Класс для генерации страницы со списком параметров
 */
class ParametersPageService extends AbstractPageService
{
    /**
     * @inheritdoc
     */
    protected $numberOfPages = 1;

    /**
     * @inheritdoc
     */
    public function getPages()
    {
        $pages = [];
        $context = $this->getContext();
        $content = $this->render($context);
        $pages[] = $this->createPage($context['outputFile'], $content);
        return $pages;
    }

    /**
     * Подготовка данных для передачи в шаблон
     *
     * @return array
     */
    protected function getContext()
    {
        $context = [];
        $config = $this->config;
        $swagger = $config->getSwagger();
        $schemaHelper = $config->getSchemaHelper();
        if (!empty($swagger->parameters)) {
            $context['outputFile'] = $this->getOutputFile();
            $context['parameters'] = $schemaHelper->prepareParameters($swagger->parameters);
        }

        //Передаем другим расширениям, чтобы они могли модифицировать данные
        $context = $this->updateContextWithExtensions($context);

        return $context;
    }

    /**
     * Путь до сохраняемого файла
     *
     * @return string
     */
    protected function getOutputFile()
    {
        return DIRECTORY_SEPARATOR . 'parameters' . DIRECTORY_SEPARATOR . 'README.md';
    }
}