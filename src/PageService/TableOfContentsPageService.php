<?php

namespace moslibs\SwaggerMD\PageService;

/**
 * Класс для генерации страницы с оглавлением
 */
class TableOfContentsPageService extends AbstractPageService
{
    /**
     * @inheritdoc
     */
    protected $numberOfPages = 1;

    /**
     * @inheritdoc
     */
    public function getPages()
    {
        $pages = [];
        $context = $this->getContext();
        $content = $this->render($context);
        $pages[] = $this->createPage($context['outputFile'], $content);

        return $pages;
    }

    /**
     * Подготовка данных для передачи в шаблон
     *
     * @return array
     */
    protected function getContext()
    {
        $context = $tags = [];

        $config = $this->config;
        $swagger = $config->getSwagger();

        if (!empty($swagger->tags)) {
            foreach ($swagger->tags as $tag) {
                if (!empty($tag->name) && !empty($tag->description)) {
                    $tags[$tag->name] = $tag->description;
                }
            }
        }

        foreach ($swagger->paths as $actionPath => $actionsList) {
            foreach ($actionsList as $method => $action) {
                if (!empty($action['tags']) && !empty($action['summary'])) {
                    $path = $this->formatPath('actions' . $actionPath);
                    $method = strtoupper($method);
                    $file = $path . '/' . $method . '.md';
                    foreach ($action['tags'] as $tag) {
                        $name = trim($tag);
                        $is_frontend = (empty($action['security'])) ? true : false;
                        $context['tags'][$name][] = [
                            'file'          => $file,
                            'title'         => $action['summary'],
                            'path'          => $actionPath,
                            'method'        => $method,
                            'is_frontend'   => $is_frontend,
                            'is_deprecated' => (!empty($action['deprecated']) && $action['deprecated'] === true) ? true : false
                        ];
                    }
                }
            }
        }

        ksort($context['tags']);
        foreach ($context['tags'] as $name => $actions) {
            usort($actions, function ($a, $b) {
                if ($a['title'] == $b['title']) {
                    return 0;
                }
                return strcmp($b['title'], $a['title']);
            });

            $context['tags'][$name] = $actions;
        }

        $context['outputFile'] = $this->getOutputFile();
        return $context;
    }

    /**
     * Путь до сохраняемого файла
     *
     * @return string
     */
    protected function getOutputFile()
    {
        return DIRECTORY_SEPARATOR . 'README.md';
    }
}