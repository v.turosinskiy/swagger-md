<?php

namespace moslibs\SwaggerMD\PageService;

/**
 * Класс для генерации страницы со списком свойств
 */
class PropertiesPageService extends AbstractPageService
{
    /**
     * @inheritdoc
     */
    protected $numberOfPages = 1;

    /**
     * @inheritdoc
     */
    public function getPages()
    {
        $pages = [];
        $context = $this->getContext();
        if (!empty($context)) {
            $content = $this->render($context);
            $pages[] = $this->createPage($context['outputFile'], $content);
        }

        return $pages;
    }

    /**
     * Подготовка данных для передачи в шаблон
     *
     * @return array
     */
    protected function getContext()
    {
        $context = [];
        $config = $this->config;
        $swagger = $config->getSwagger();
        $schemaHelper = $config->getSchemaHelper();

        if (!empty($swagger->definitions['CommonProperties'])) {
            $context['outputFile'] = $this->getOutputFile();
            $properties = $schemaHelper->prepareProperties($swagger->definitions['CommonProperties']);
            if (!empty($properties)) {
                $context['properties'] = $properties;
            }
        }

        return $context;
    }

    /**
     * Путь до сохраняемого файла
     *
     * @return string
     */
    protected function getOutputFile()
    {
        return DIRECTORY_SEPARATOR . 'properties' . DIRECTORY_SEPARATOR . 'README.md';
    }
}