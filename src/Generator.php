<?php

namespace moslibs\SwaggerMD;

use moslibs\SwaggerMD\Config\Config;
use moslibs\SwaggerMD\PageService\PageServiceInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Генератор документации
 */
class Generator implements GeneratorInterface
{
    /**
     * Настройки генератора документации
     *
     * @var Config
     */
    protected $config;

    /**
     * Путь к дирректории для сохранения документации
     *
     * @var string
     */
    protected $output;

    /**
     * Логгер для вывода сообщений
     *
     * @var OutputInterface
     */
    protected $logger;

    /**
     * Список сервисов, которые используются для создания страниц
     *
     * @var PageServiceInterface[]
     */
    protected $pageServices;

    /**
     * Прогресс бар для отображения процесса генерации страниц
     *
     * @var ProgressBar
     */
    protected $progressBar;

    /**
     * Инициализация генератора
     *
     * @param Config $config - настройки генератора документации
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * Добавление логгера для вывода сообщений
     *
     * @param OutputInterface $logger
     */
    public function setLogger(OutputInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    public function getNumberOfPages()
    {
        $numberOfPages = 0;
        $pageServices = $this->getPageServices();

        foreach ($pageServices as $pageService) {
            $numberOfPages += $pageService->getNumberOfPages();
        }

        return $numberOfPages;
    }

    /**
     * Возвращает список сервисов
     *
     * @return PageServiceInterface[]
     */
    public function getPageServices()
    {
        return $this->pageServices;
    }

    /**
     * Добавление сервиса
     *
     * @param PageServiceInterface $pageService
     */
    public function addPageService($pageService)
    {
        $this->pageServices[] = $pageService;
    }

    /**
     * Генерация документации
     *
     * @param string $buildDir - директория куда сохранять файлы
     */
    public function generatePages($buildDir)
    {
        $pageServices = $this->getPageServices();
        $progressBar = $this->getProgressBar();
        $buildDir = rtrim($buildDir, DIRECTORY_SEPARATOR);
        $fileOutput = $this->config->getFileOutput();

        if ($progressBar instanceof ProgressBar) {
            $this->logger->write(PHP_EOL);
            $progressBar->start();
        }

        // поочередно вызываем сервисы
        foreach ($pageServices as $pageService) {
            //сервисы генерируют страницы в виде обьектов
            $pages = $pageService->getPages();
            foreach ($pages as $page) {
                //поочередно сохраняем страницы и обновляем прогрес бар
                $file = $buildDir . $page->getOutputFile();
                $fileOutput->save($file, $page->getContent());
                if (isset($this->progressBar)) {
                    if ($progressBar instanceof ProgressBar) {
                        $progressBar->advance();
                    }
                }
            }
        }

        if ($progressBar instanceof ProgressBar) {
            $progressBar->finish();
            $this->logger->writeln(PHP_EOL);
        }
    }

    /**
     * Получение индикатора выполнения
     *
     * @return ProgressBar|null
     */
    protected function getProgressBar()
    {
        return $this->progressBar;
    }

    public function withProgressBar(ProgressBar $progressbar)
    {
        $this->progressBar = $progressbar;
    }
}
