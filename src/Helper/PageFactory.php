<?php

namespace moslibs\SwaggerMD\Helper;

use moslibs\SwaggerMD\Page\Page;

/**
 * Фабрика страниц
 */
class PageFactory
{
    /**
     * @inheritdoc
     */
    public function createPage($file, $content)
    {
        return new Page($file, $content);
    }
}
