<?php

namespace moslibs\SwaggerMD\Helper;

/**
 * Помощник для обработки полей перед созданием таблицы в формате Markdown
 */
class TableHelper
{
    /**
     * Создание таблиц с выводом полей из параметра $columns
     *
     * @param array $items   - неотфильтрованный список свойств в формате ["title" => "Первое свойство", ...]
     * @param array $headers - перечисление заголовков в формате ["title" => "Название", ...]
     * @return string
     */
    public function renderItemsAsTable($items, $headers)
    {
        $str = '';

        if (!empty($items)) {
            $headerKeys = $this->getColumnKeys($items, $headers);
            $rows = $this->getRows($items, $headerKeys);
            foreach ($headers as $key => $header) {
                if (!in_array($key, $headerKeys)) {
                    unset($headers[$key]);
                }
            }

            $str = $this->renderTable($headers, $rows);
        }

        return $str;
    }

    /**
     * @param $items   array
     * @param $columns array
     * @return array
     */
    protected function getColumnKeys($items, $columns)
    {
        $columnKeys = [];

        foreach ($items as $item) {
            foreach ($columns as $key => $column) {
                if (!empty(array_key_exists($key, $item)) && !in_array($key, $columnKeys)) {
                    $columnKeys[] = $key;
                }
            }
        }

        return $columnKeys;
    }

    /**
     * @param array $items
     * @param array $columns
     * @return array
     */
    protected function getRows($items, $columns)
    {
        $rows = [];
        foreach ($items as $item) {
            $row = [];
            foreach ($columns as $fieldKey) {
                $row[$fieldKey] = $this->formatField($fieldKey, $item);
            }
            $rows[] = $row;
        }
        return $rows;
    }

    /**
     * Форматирует значения для свойств
     *
     * @param string $fieldKey
     * @param array  $item
     * @return null|string
     */
    protected function formatField($fieldKey, $item)
    {
        $value = null;

        switch ($fieldKey) {
            case 'filterable':
                if (array_key_exists('filterable', $item)) {
                    $value = ($item['filterable'] === true) ? '+' : '-';
                } else {
                    $value = '+';
                }

                break;
            case 'sortable':
                if (array_key_exists('sortable', $item)) {
                    $value = ($item['sortable'] === true) ? '+' : '-';
                } else {
                    $value = '+';
                }
                break;
            case 'optional':
                if (array_key_exists('optional', $item)) {
                    if (is_bool($item[$fieldKey])) {
                        $value = ($item[$fieldKey] === true) ? '+' : '-';
                    } else {
                        $value = $item[$fieldKey];
                    }

                } else {
                    $value = '+';
                }
                break;
            default:
                $value = (!empty($item[$fieldKey])) ? $item[$fieldKey] : 'Не указано';
        }

        return $value;
    }

    /**
     * Создание таблиц в виде строки
     *
     * @param array $headers
     * @param array $rows
     * @return string
     */
    public function renderTable($headers, $rows)
    {
        $t = new TextTable($headers, $rows);
        return $t->render();
    }
}