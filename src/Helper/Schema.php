<?php

namespace moslibs\SwaggerMD\Helper;

use Swagger\Annotations\Swagger;

/**
 * Помощник для работы со схемами
 */
class Schema
{
    /**
     * Дополнительные поля
     *
     * @var array
     */
    protected $extraFields = [
        'default',
        'full_description',
        'enum',
        'optional_description',
        'minLength',
        'maxLength'
    ];

    /**
     * Документ в виде обьекта с OpenAPI спецификацией
     *
     * @var Swagger
     */
    private $swagger;

    /**
     * @param Swagger $swagger
     */
    public function __construct(Swagger $swagger)
    {
        $this->swagger = $swagger;
    }

    /**
     * Возвращает массив с примером схемы
     *
     * @param $schema
     * @return array|null
     * @throws \Exception
     */
    public function getExampleBySchema($schema)
    {
        $examples = null;
        if (!empty($schema['example'])) {
            $examples = $schema['example'];
        } elseif (!empty($schema['default'])) {
            $examples = $schema['default'];
        } elseif (!empty($schema['allOf'])) {
            $examples = [];
            foreach ($schema['allOf'] as $childSchema) {
                $examples = array_merge($examples, $this->getExampleBySchema($childSchema));
            }
        } elseif (!empty($schema['$ref'])) {
            $model = $this->getSwagger()->ref($schema['$ref']);
            $examples = $this->getExampleByModel($model);
        } elseif (!empty($schema['properties'])) {
            $examples = $this->getExampleByModel($schema);
        } elseif (!empty($schema['type'])) {
            if (!empty($schema['items'])) {
                $examples = $this->getExampleByItems($schema['items']);
            } else {
                $examples = $schema['type'];
            }
        }
        return $examples;
    }

    /**
     * @return Swagger
     */
    protected function getSwagger()
    {
        return $this->swagger;
    }

    /**
     * Возвращает массив с примером схемы модели
     *
     * @param $model
     * @return array|null
     * @throws \Exception
     */
    public function getExampleByModel($model)
    {
        $example = null;
        if (!empty($model['example'])) {
            $example = $model['example'];
        } elseif (!empty($model['default'])) {
            $example = $model['default'];
        } elseif (!empty($model['properties'])) {
            $example = [];
            foreach ($model['properties'] as $key => $property) {
                $example[$key] = $this->getExampleByProperty($property);
            }
        } elseif (!empty($property['type'])) {
            $example = $this->getExampleByProperty($property);
        }
        return $example;
    }

    /**
     * Возвращает массив с примером схемы свойства
     *
     * @param $property
     * @return array|null
     * @throws \Exception
     */
    public function getExampleByProperty($property)
    {
        $example = null;
        if (!empty($property['example'])) {
            $example = $property['example'];
        } elseif (!empty($property['default'])) {
            $example = $property['default'];
        } elseif (!empty($property['properties'])) {
            $example = $this->getExampleByModel($property);
        } elseif (!empty($property['items'])) {
            $example = $this->getExampleByItems($property['items']);
        } elseif (!empty($property['type'])) {
            if (!empty($property['enum'])) {
                $example = $property['enum'][0];
            } else {
                $example = $property['type'];
            }
        } elseif (!empty($property['$ref'])) {
            preg_match("/(.*)\/properties\/(.*)/", $property['$ref'], $output_array);
            if (!empty($output_array)) {
                $ref = $output_array[1];
                $propertyName = $output_array[2];
                $model = $this->getSwagger()->ref($ref);
                $example = $this->getExampleByProperty($model['properties'][$propertyName]);
            } else {
                $model = $this->getSwagger()->ref($property['$ref']);
                $example = $this->getExampleByModel($model);
            }
        }
        return $example;
    }

    /**
     * Возвращает массив с примером схемы Items
     *
     * @param     $items
     * @param int $count
     * @return array|null
     * @throws \Exception
     */
    public function getExampleByItems($items, $count = 1)
    {
        $examples = null;
        if (!empty($items['$ref'])) {
            $model = $this->getSwagger()->ref($items['$ref']);
            $example = $this->getExampleByModel($model);
        } elseif (!empty($items['allOf'])) {
            $example = [];
            foreach ($items['allOf'] as $childSchema) {
                $example = array_merge($example, $this->getExampleBySchema($childSchema));
            }
        } elseif (!empty($items['properties'])) {
            $example = $this->getExampleByModel($items);
        } elseif (!empty($items['type'])) {
            if ($items['type'] == 'array') {
                $example = $this->getExampleByItems($items['items']);
            } else {
                $example = $this->getExampleByProperty($items);
            }
        }

        if (!empty($example)) {
            for ($i = 0; $i < $count; $i++) {
                $examples[] = $example;
            }
        }
        return $examples;
    }

    /**
     * Подготовка параметров
     *
     * @param array $parameters
     * @return array
     */
    public function prepareParameters($parameters)
    {
        if (!empty($parameters)) {
            foreach ($parameters as $key => $parameter) {
                $parameter = array_merge($this->getPropertyByRef($parameter), $parameter);
                if (!empty($parameter['description'])) {
                    $splitDesc = $this->splitDescription($parameter['description']);
                    $parameter = array_merge($parameter, $splitDesc);
                }

                if (empty($parameter['short_name'])) {
                    $parameter['short_name'] = $parameter['name'];
                }
                $parameters[$key] = $parameter;
            }
        }

        return $parameters;
    }

    /**
     * Получение данных о свойстве через ссылку
     *
     * @param $property array - массив с описанием свойства
     * @return array
     */
    public function getPropertyByRef($property)
    {
        $swagger = $this->getSwagger();
        if (!empty($property['$ref']) && stristr($property['$ref'], '#/')) {
            $refProperty = $swagger->ref($property['$ref']);
            preg_match("/#\/definitions\/CommonTypes\/properties\/(.+)/", $property['$ref'], $output_array);
            if (!empty($output_array[1])) {
                $property['type'] = $output_array[1];
                switch ($output_array[1]) {
                    case 'datetimeMaterial':
                    case 'datetimeDefault':
                        $property['type'] = 'datetime';
                        break;
                    case 'dateDefault':
                        $property['type'] = 'date';
                        break;
                    default:
                        $property['type'] = $output_array[1];
                }
            } else {
                preg_match("/#\/definitions\/CommonProperties\/properties\/(.+)/", $property['$ref'], $output_array);
                if (!empty($output_array[1])) {
                    if (!empty($refProperty['$ref'])) {
                        $ref = $refProperty['$ref'];
                    } elseif (!empty($refProperty['type']) && $refProperty['type'] == 'array' && !empty($refProperty['items']['$ref'])) {
                        $ref = $refProperty['items']['$ref'];
                    }

                    if (!empty($ref)) {
                        preg_match("/#\/definitions\/([^\/]+)$/", $ref, $ref_output_array);
                        if (!empty($ref_output_array[1]) && $ref_output_array[1] != 'CommonTypes') {
                            $property['readmeFile'] = '/models/' . $ref_output_array[1] . '/README.md';
                        }
                    }
                }
            }
            unset($property['$ref']);
            if (!empty($refProperty['$ref'])) {
                $refProperty = $this->getPropertyByRef($refProperty);
            }
            $property = array_merge($refProperty, $property);
        }

        if (!empty($property['model_path'])) {
            $property['type'] = ($property['type'] == 'array') ? 'array object' : 'object';
        } elseif (!empty($property['type']) && !empty($property['enum'])) {
            $property['type'] = 'enum ' . $property['type'];
            if (empty(array_diff($property['enum'], [0, 1]))) {
                unset($property['enum']);
            }
        } elseif (!empty($property['type']) && $property['type'] == 'array') {
            if (!empty($property['items']['ref'])) {
                $refProperty = $this->getPropertyByRef($property['items']['ref']);
                $property['items'] = array_merge($refProperty, $property['items']);
            }
            if (!empty($property['items']['enum'])) {
                $property['array_enum'] = $property['items']['enum'];
                $property['type'] = 'array enum ' . $property['items']['type'];
            }
        }

        if (!stristr($property['type'],
                '/ null') && !empty($property['x-nullable']) && $property['x-nullable'] === true) {
            $property['type'] .= ' / null';
        }

        if (!empty($property['description'])) {
            $description = $this->splitDescription($property['description']);
            $property = array_merge($property, $description);
        }

        return $property;
    }

    /**
     * Разделение строки на две части с разделителем в виде перехода строки
     *
     * @param $description
     * @return array
     */
    public function splitDescription($description)
    {
        $result = [];
        $pos = strpos($description, "\n");
        if (!empty($pos)) {
            $result['short_name'] = trim(substr($description, 0, $pos));
            $result['full_description'] = trim(substr($description, $pos, strlen($description)));
        } else {
            $result['short_name'] = $description;
        }
        return $result;
    }

    /**
     * Подготовка ответов
     *
     * @param array $responses
     * @return array
     */
    public function prepareResponses($responses)
    {
        foreach ($responses as $code => $response) {
            if (!empty($response['schema'])) {
                $model = $this->getModelBySchema($response['schema']);
                if (!empty($model['properties'])) {
                    $properties = $this->prepareProperties($model);
                    $responses[$code]['properties'] = $properties;
                }
            }
        }
        return $responses;
    }

    /**
     * Получения списка свойств для модели на основании схемы
     *
     * @param $schema
     * @return array
     * @throws \Exception
     */
    public function getModelBySchema($schema)
    {
        $model = [];

        if (!empty($schema['properties']['items']['items']['$ref'])) {
            $model = $this->getSwagger()->ref($schema['properties']['items']['items']['$ref']);
        } elseif (!empty($schema['properties']['fields']['items']['properties'])) {
            $model = $schema['properties']['fields']['items'];
        } elseif (!empty($schema['properties']['items']['items']['properties'])) {
            $model = $schema['properties']['items']['items'];
        } elseif (!empty($schema['$ref'])) {
            $model = $this->getSwagger()->ref($schema['$ref']);
        } elseif (!empty($schema['allOf']) || !empty($schema['properties']['items']['items']['allOf'])) {
            $elementSchema = (!empty($schema['properties']['items']['items']['allOf'])) ? $schema['properties']['items']['items']['allOf'] : $schema['allOf'];
            $properties = [];
            foreach ($elementSchema as $childSchema) {
                if (!empty($childSchema['$ref'])) {
                    $refModel = $this->getSwagger()->ref($childSchema['$ref']);
                    $properties = array_merge($properties, $refModel['properties']);
                } elseif (!empty($childSchema['properties'])) {
                    $properties = array_merge($properties, $childSchema['properties']);
                }
            }
            if (!empty($properties)) {
                $model = [
                    'properties' => $properties
                ];
            }
        } elseif (!empty($schema['properties'])) {
            $model = $schema;
        }

        return $model;
    }

    /**
     * Подготовка свойств
     *
     * @param array $model
     * @return array
     */
    public function prepareProperties($model)
    {
        $properties = [];
        if (!empty($model['properties'])) {
            foreach ($model['properties'] as $key => $property) {
                $property = $this->getPropertyByRef($property);

                $property['name'] = $key;

                $property['sortable'] = (!empty($property['x-sortable'])) ? (bool)($property['x-sortable']) : true;
                $property['filterable'] = (!empty($property['x-filterable'])) ? (bool)($property['x-filterable']) : true;

                if (!empty($property['description'])) {
                    $splitDesc = $this->splitDescription($property['description']);
                    $property = array_merge($property, $splitDesc);
                }

                if (empty($property['short_name'])) {
                    $property['short_name'] = $key;
                }
                $properties[$key] = $property;
            }
        }

        return $properties;
    }

    protected function getExtraFields()
    {
        return $this->extraFields;
    }

    /**
     * Подготовка моделей
     *
     * @param array $definition
     * @return array
     * @throws \Exception
     */
    public function prepareDefinition($definition)
    {
        $definition = $this->getModelBySchema($definition);
        if (!empty($definition['properties'])) {
            $definition['properties'] = $this->prepareProperties($definition);
        }

        return $definition;
    }

    /**
     * Путь до файла с документацией модели по ее ключу
     *
     * @param string $title
     * @return string
     */
    public function getDocPathByDefinitionTitle($title)
    {
        return DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . $title . DIRECTORY_SEPARATOR . 'README.md';
    }
}