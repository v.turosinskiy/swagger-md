<?php

namespace moslibs\SwaggerMD\Helper;

/**
 * Класс для рендеринга таблиц в Markdown формате
 */
class TextTable
{
    /**
     * Заголовки столбцов
     *
     * @var array
     */
    protected $headers = [];

    /**
     * Строки
     *
     * @var array
     */
    protected $rows = [];

    /**
     * Список максимальной ширины колонок
     *
     * @var array
     */
    protected $maxColWidths = [];

    /**
     * Максимальная ширина всей таблицы
     *
     * @var int
     */
    protected $maxWidth = 0;

    /**
     * Массив с ссылками на элементы внутри таблицы
     * Добавляется в конец таблицы
     *
     * @var array
     */
    protected $links;

    /**
     * Конструктор
     *
     * @param array $headers
     * @param array $rows
     */
    public function __construct($headers, $rows)
    {
        $this->headers = $headers;
        $this->rows = $rows;
        $maxColWidths = [];
        foreach ($headers as $key => $header) {
            $maxColWidth = mb_strlen($header);
            foreach ($rows as $row) {
                $width = (is_array($row[$key])) ? mb_strlen($row[$key]['value']) : mb_strlen($row[$key]);
                if ($width > $maxColWidth) {
                    $maxColWidth = $width;
                }
            }

            $maxColWidths[$key] = $maxColWidth;
            $this->maxWidth += $maxColWidth + 3;
        }
        $this->maxWidth -= 1;
        $this->maxColWidths = $maxColWidths;
    }

    public function render()
    {
        $str = $this->renderHeaders();
        $str .= $this->renderRows();
        $str .= $this->renderLinks();
        return $str;
    }

    /**
     * Возвращает первую строку со списком названий колонок у таблицы
     *
     * @return string
     */
    protected function renderHeaders()
    {
        $str = '';
        foreach ($this->headers as $key => $header) {
            $str .= $this->renderHeader($key);
        }
        $str .= '|' . PHP_EOL;
        foreach ($this->maxColWidths as $maxColWidth) {
            $str .= '|:' . str_repeat('-', $maxColWidth + 1);
        }
        $str .= '|' . PHP_EOL;
        return $str;
    }

    /**
     * Возвращает заголовок колонки таблицы
     *
     * @param $key
     * @return string
     */
    protected function renderHeader($key)
    {
        $header = $this->headers[$key];
        $multiplier = $this->maxColWidths[$key] - mb_strlen($header);
        return '| ' . $header . ' ' . str_repeat(' ', $multiplier);
    }


    /**
     * Возвращает строки таблицы
     *
     * @return string
     */
    protected function renderRows()
    {
        $str = '';
        foreach ($this->rows as $row) {
            $str .= $this->renderRow($row);
        }

        return $str;
    }

    /**
     * Возвращает отдельную строку таблицы
     *
     * @param array $row
     * @return string
     */
    protected function renderRow($row)
    {
        $str = '';
        $headers = $this->headers;
        foreach ($headers as $key => $value) {
            $rowData = $row[$key];
            if (is_array($rowData)) {
                $rowValue = $rowData['value'];
                $this->links[$rowValue] = $rowData['link'];
            } else {
                $rowValue = $rowData;
            }
            $multiplier = $this->maxColWidths[$key] - mb_strlen($rowValue);
            $str .= '| ' . $rowValue . ' ' . str_repeat(' ', $multiplier);
        }
        $str .= '|' . PHP_EOL;

        return $str;
    }

    /**
     * Вывод ссылок на элементы внутри таблицы
     *
     * @return string
     */
    protected function renderLinks()
    {
        $str = '';
        if (!empty($this->links)) {
            $str .= PHP_EOL;
            foreach ($this->links as $key => $link) {
                $str .= $key . ': ' . $link . PHP_EOL;
            }
        }

        return $str;
    }
}