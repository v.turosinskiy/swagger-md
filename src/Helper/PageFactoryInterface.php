<?php

namespace moslibs\SwaggerMD\Helper;

use moslibs\SwaggerMD\Page\Page;

/**
 * Фабрика страниц
 */
interface PageFactoryInterface
{
    /**
     * @param string $file    - путь до файла для записи
     * @param string $content - данные для записи в файл
     * @return Page
     */
    public function createPage($file, $content);
}
