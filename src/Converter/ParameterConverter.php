<?php

namespace moslibs\SwaggerMD\Converter;

class ParameterConverter extends AbstractConverter
{
    protected $fields = [
        'ref',
        'parameter',
        'name',
        'in',
        'description',
        'required',
        'schema',
        'type',
        'format',
        'allowEmptyValue',
        'items',
        'collectionFormat',
        'default',
        'maximum',
        'exclusiveMaximum',
        'minimum',
        'exclusiveMinimum',
        'maxLength',
        'minLength',
        'pattern',
        'maxItems',
        'minItems',
        'uniqueItems',
        'enum',
        'multipleOf',
    ];

    protected $required = [
        'name',
        'in',
    ];

    protected $hidden = [
        'items',
    ];

    public function convertToArray($data)
    {
        $fields = [];
        foreach ($data as $field => $value) {
            if (!empty(in_array($field, $value))) {
                $fields[$field] = $value;
            } else {
                $fields[$field] = null;
            }
        }

        $result = [
            'fields'   => $fields,
            'required' => $this->required,
            'hidden'   => $this->hidden
        ];

        return $result;
    }
}