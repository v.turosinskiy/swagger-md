<?php

namespace moslibs\SwaggerMD\Converter;

abstract class AbstractConverter implements ConverterInterface
{
    public $required = [];
    public $fields   = [];
    public $hidden   = [];

    public function addField($field, $required = false, $hidden = false)
    {
        if (!in_array($field, $this->fields)) {
            $fields[] = $field;
        }

        if ($required) {
            if (!in_array($field, $this->required)) {
                $this->required[] = $field;
            }
        } else {
            if (in_array($field, $this->required)) {
                $key = array_search($field, $this->required);
                unset($required[$key]);
            }
        }

        if ($hidden) {
            if (!in_array($field, $this->hidden)) {
                $this->hidden[] = $field;
            }
        } else {
            if (in_array($field, $this->hidden)) {
                $key = array_search($field, $this->hidden);
                unset($hidden[$key]);
            }
        }
    }

    abstract function convertToArray($data);
}