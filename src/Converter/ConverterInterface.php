<?php

namespace moslibs\SwaggerMD\Converter;

interface ConverterInterface
{
    public function addField($field, $required = false, $hidden = false);

    public function convertToArray($data);
}