<?php

namespace moslibs\SwaggerMD;

/**
 * Генератор документации
 */
interface GeneratorInterface
{
    /**
     * Генерация документации
     *
     * @param $output - директория куда сохранять файлы
     */
    public function generatePages($output);

    /**
     * Возвращает количество страниц, которое будет сгенерировано
     * всеми зарегистрированными сервисами.
     *
     * @return int
     */
    public function getNumberOfPages();
}