<?php

namespace moslibs\SwaggerMD\Command;

use moslibs\SwaggerMD\Exception\InvalidParameterException;
use moslibs\SwaggerMD\Exception\JsonNotFoundException;
use moslibs\SwaggerMD\Exception\ParameterNotFoundException;
use moslibs\SwaggerMD\Generator;
use Swagger\Annotations\Swagger;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Эта команда отвечает за создание документации в виде Markdown файлов
 * с помощью json файла в формате Open API 2 спецификации
 */
class MarkdownCommand extends AbstractCommand
{
    /**
     * Путь к json файлу
     *
     * @var string
     */
    protected $json;

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('swgmd-md')->setDefinition([
            $this->getBuildOption(),
            $this->getJsonFileOption(),
            $this->getDryRunOption(),
        ])->setDescription('Create docs')->setHelp(<<<'EOF'
EOF
        );
    }

    /**
     * @inheritdoc
     */
    protected function executeBody(InputInterface $input, OutputInterface $output)
    {
        //Загрузка DI
        $this->loadContainerBuilder($output);
        $output->writeln($this->line);

        $output->writeln('Load parameters... ');

        //Проверка: Требуется ли создание файлов?
        $output->writeln('Dry-run:');
        $dryRun = $this->checkDryRunMode($input->getOption('dry-run'));
        $output->writeln('  - ' . (($dryRun) ? 'Enabled (Nothing will be changed)' : 'Disabled'));

        $buildDir = $input->getOption('build');
        $buildDir = $this->getParameterByMask($buildDir);
        if (!empty($buildDir)) {
            $output->writeln($this->line);
            $output->writeln('Build documentation directory: ');
            $output->writeln('  - ' . $buildDir);
        }

        $swaggerJson = $input->getOption('json-file');
        $swaggerJson = $this->getParameterByMask($swaggerJson);
        if (!empty($swaggerJson)) {
            $output->writeln($this->line);
            $output->writeln('Swagger json file: ');
            $output->writeln('  - ' . $swaggerJson);
        }
        $output->writeln($this->line);

        //Загрузка Swagger
        $output->writeln('Load content from json...');
        $this->loadSwaggerByJson($swaggerJson);
        $output->writeln($this->line);

        $output->writeln('Generator Initialization...');
        $output->writeln($this->line);

        $output->writeln('Running Markdown file generation...');
        $generator = $this->containerBuilder->get('generator');

        /**
         * @var Generator $generator
         */
        $buildDir = $input->getOption('build');
        $buildDir = $this->getParameterByMask($buildDir);

        $generator->generatePages($buildDir);
    }

    /**
     * Инициализация парсера Swagger из json по формату Open Api
     *
     * @param string $json - путь к json файлу
     * @throws InvalidParameterException
     * @throws JsonNotFoundException
     * @throws ParameterNotFoundException
     */
    protected function loadSwaggerByJson($json)
    {
        $properties = $this->getPropertiesByJson($json);
        $swagger = new Swagger($properties);
        $this->containerBuilder->set('swagger', $swagger);
    }

    /**
     * Получение массива $properties из json в формате Open Api
     *
     * @param string $json - путь к json файлу
     * @return mixed
     * @throws JsonNotFoundException
     * @throws InvalidParameterException
     * @throws ParameterNotFoundException
     */
    protected function getPropertiesByJson($json)
    {
        $json = $this->getParameterByMask($json);

        // инициализация парсера аннотаций swagger
        if (file_exists($json)) {
            $content = file_get_contents($json);
        } else {
            throw new JsonNotFoundException($json);
        }

        $properties = json_decode($content, true);
        return $properties;
    }
}
