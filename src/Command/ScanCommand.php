<?php

namespace moslibs\SwaggerMD\Command;

use moslibs\SwaggerMD\Command\Traits\SnippetTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Эта команда отвечает за создание json файла в формате Open API 2 спецификации
 */
class ScanCommand extends AbstractCommand
{
    use SnippetTrait;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->ignoreValidationErrors();

        $this->setName('swgmd-scan')->setDefinition([
            $this->getBuildOption(),
            $this->getScanOption(),
            $this->getExcludeOption(),
            $this->getJsonFileOption(),
            $this->getDryRunOption(),
            $this->getDelimiterOption(),
        ])->setDescription('Create swagger.json')->setHelp(<<<'EOF'
EOF
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function executeBody(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Load container builder...');
        $this->loadContainerBuilder($output);
        $output->writeln($this->line);

        $output->writeln('Load parameters...');

        $output->writeln('Dry-run:');
        $dryRun = $this->checkDryRunMode($input->getOption('dry-run'));
        $output->writeln('  - ' . (($dryRun) ? 'Enabled (Nothing will be changed)' : 'Disabled'));

        $output->writeln('Delimiter:');
        $delimiter = $this->formatParameter($input->getOption('delimiter'));
        $output->writeln('  - ' . $delimiter);

        $swgJsonFile = $this->getParameterByMask($input->getOption('json-file'));
        $output->writeln('Save json to:');
        $output->writeln('  - ' . $swgJsonFile);

        $exclude = $this->getParameterAsArrayByMask($input->getOption('exclude'));
        if (!empty($exclude)) {
            $output->writeln('Exclude from scan: ');
            foreach ($exclude as $dir) {
                $output->writeln('  - ' . $dir);
            }
        }

        $dirs = $this->getParameterAsArrayByMask($input->getOption('scan'), $delimiter);
        $output->writeln('Scan directories:');

        foreach ($dirs as $dir) {
            $output->writeln('  - ' . $dir);
        }

        $output->writeln($this->line);

        $output->writeln('Create json file...');
        $swagger = \Swagger\scan($dirs, ['exclude' => $exclude]);
        $output->writeln($this->line);
        $output->writeln('Saving file...');

        $content = $swagger->__toString();

        if ($this->containerBuilder->hasParameter('snippets.replace')) {
            $snippetReplaceParameter = $this->containerBuilder->getParameter('snippets.replace');
            $snippetReplaceFile = $this->formatParameter($snippetReplaceParameter);
            $content = $this->formatString($snippetReplaceFile, $content);
        }

        $fileOutput = $this->containerBuilder->get('file_output');
        $fileOutput->save($swgJsonFile, $content);
    }
}
