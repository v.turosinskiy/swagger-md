<?php

namespace moslibs\SwaggerMD\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Эта команда отображает основные настройки,
 * которые будут использоваться при вызове других команд
 */
class ConfigCommand extends AbstractCommand
{
    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->ignoreValidationErrors();

        $this->setName('swgmd-conf')->setDescription('Get the current SwaggerMD configuration')->setDefinition([
            $this->getBuildOption(),
            $this->getJsonFileOption(),
            $this->getDryRunOption(),
            $this->getScanOption(),
            $this->getExcludeOption(),
            $this->getDelimiterOption()
        ])->setHelp(<<<'EOF'
EOF
        );
    }

    /**
     * @inheritdoc
     */
    protected function executeBody(InputInterface $input, OutputInterface $output)
    {
        $this->loadContainerBuilder($output);
        $scan = $input->getOption('scan');
        $buildDir = $input->getOption('build');
        $swaggerJson = $input->getOption('json-file');
        $exclude = $input->getOption('exclude');
        $delimiter = $input->getOption('delimiter');

        $delimiter = $this->formatParameter($delimiter);
        $scanDirs = $this->getParameterAsArrayByMask($scan, $delimiter);
        if (!empty($scanDirs)) {
            $output->writeln($this->line);
            $output->writeln('Scanning directories: ');
            foreach ($scanDirs as $dir) {
                $output->writeln('  - ' . $dir);
            }
        }

        $excludeDirs = $this->getParameterAsArrayByMask($exclude, $delimiter);
        if (!empty($excludeDirs)) {
            $output->writeln($this->line);
            $output->writeln('Exclude directories: ');
            foreach ($excludeDirs as $dir) {
                $output->writeln('  - ' . $dir);
            }
        }

        $buildDir = $this->getParameterByMask($buildDir);
        if (!empty($buildDir)) {
            $output->writeln($this->line);
            $output->writeln('Build documentation directory: ');
            $output->writeln('  - ' . $buildDir);
        }

        $swaggerJson = $this->getParameterByMask($swaggerJson);
        if (!empty($swaggerJson)) {
            $output->writeln($this->line);
            $output->writeln('Swagger json file: ');
            $output->writeln('  - ' . $swaggerJson);
        }
    }
}