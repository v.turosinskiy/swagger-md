<?php

namespace moslibs\SwaggerMD\Command\Traits;

use Symfony\Component\Console\Output\OutputInterface;

/**
 *
 * Общий трейт для вывода сообщений о начале и завершении выполнении команды
 */
trait MessagesTrait
{
    protected $line = '------------------------------------';

    /**
     * Сообщение перед началом выполнения команды
     *
     * @param OutputInterface $output
     */
    protected function outputTitleMessage(OutputInterface $output)
    {
        $output->writeln('Start of execution - ' . $this->getName());
    }

    /**
     * Сообщение после завершения выполнения команды
     *
     * @param OutputInterface $output
     */
    protected function outputFooterMessage(OutputInterface $output)
    {
        $output->writeln('End of execution - ' . $this->getName());
        $output->writeln($this->line);
    }
}
