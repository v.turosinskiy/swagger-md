<?php

namespace moslibs\SwaggerMD\Command\Traits;

use Symfony\Component\Console\Input\InputOption;

/**
 * Содержит описание параметров, которые используются при создании документации
 */
trait OptionsTrait
{
    /**
     * Возвращает опцию для указания пути директории с документацией
     *
     * @return InputOption
     */
    protected function getBuildOption()
    {
        return new InputOption('build', 'b', InputOption::VALUE_REQUIRED, 'Directory with Markdown Documentation',
            '%swagger.build_dir%');
    }

    /**
     * Возвращает опцию для указания пути до файл с OpenAPI спецификацией
     *
     * @return InputOption
     */
    protected function getJsonFileOption()
    {
        return new InputOption('json-file', 'j', InputOption::VALUE_REQUIRED, 'Swagger json file',
            '%swagger.swagger_json%');
    }

    /**
     * Возвращает опцию для указания списка дирректорий для поиска аннотаций
     *
     * @return InputOption
     */
    protected function getScanOption()
    {
        return new InputOption('scan', 's', InputOption::VALUE_REQUIRED, 'Scanning directories', '%swagger.scan%');
    }

    /**
     * Возвращает опцию для запуска в холостом режиме
     *
     * @return InputOption
     */
    protected function getDryRunOption()
    {
        return new InputOption('dry-run', null, InputOption::VALUE_OPTIONAL, 'Without write to file', false);
    }

    /**
     * Возвращает опцию для указания списка дирректорий,
     * которые должны быть исключены из поиска аннотаций
     *
     * @return InputOption
     */
    protected function getExcludeOption()
    {
        return new InputOption('exclude', '-e', InputOption::VALUE_OPTIONAL, 'Exclude path from scan',
            '%swagger.exclude%');
    }

    /**
     * Возвращает опцию для определения символа разделителя
     *
     * @return InputOption
     */
    protected function getDelimiterOption()
    {
        return new InputOption('delimiter', '-d', InputOption::VALUE_REQUIRED,
            'Delimiter for options values(scan, exclude)', '%swagger.delimiter%');
    }
}
