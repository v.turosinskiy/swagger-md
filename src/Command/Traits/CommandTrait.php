<?php

namespace moslibs\SwaggerMD\Command\Traits;

use moslibs\SwaggerMD\Exception\AutoloaderNotFoundException;
use moslibs\SwaggerMD\Exception\ConfigNotFoundException;
use moslibs\SwaggerMD\Exception\InvalidParameterException;
use moslibs\SwaggerMD\Exception\ParameterNotFoundException;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * Помощник для инициализация DI и загрузки файлов настроек
 */
trait CommandTrait
{
    /**
     * Строитель контейнеров
     *
     * @var ContainerBuilder
     */
    protected $containerBuilder;

    /**
     * Путь до корневой дирректории проекта
     *
     * @var string
     */
    protected $root;

    /**
     * Метод для проверки запущена ли команда в тестовом режиме
     *
     * @param $dryRun - если false, то файлы не будут создаваться
     * @return bool
     */
    protected function checkDryRunMode($dryRun)
    {
        $isEnabled = false;
        if ($dryRun !== false) {
            //Использование заглушки для создания файлов в режиме dry-run
            $this->containerBuilder->setParameter('file_output.class', '%file_output.fake_class%');
            $isEnabled = true;
        }
        return $isEnabled;
    }

    /**
     * Извлекает данные из строки и возвращает массив параметров
     *
     * @param        $input
     * @param string $delimiter
     * @return array
     * @throws InvalidParameterException
     * @throws ParameterNotFoundException
     */
    protected function getParameterAsArrayByMask($input, $delimiter = ',')
    {
        $parameters = [];

        if (is_string($input)) {
            $input = $this->getParameterByMask($input);
        }

        if (is_string($input)) {
            $input = explode($delimiter, $input);
        }

        if (!empty($input) && is_array($input)) {
            foreach ($input as $parameter) {
                $parameters[] = $this->getParameterByMask($parameter);
            }
        }
        return $parameters;
    }

    /**
     * Поиск параметра example по примеру %example%
     *
     * @param $inputLine
     * @return mixed|string
     * @throws InvalidParameterException
     * @throws ParameterNotFoundException
     */
    protected function getParameterByMask($inputLine)
    {
        preg_match("/^%(.+)%$/", $inputLine, $outputArray);
        if (!empty($outputArray[1])) {
            $key = $outputArray[1];
            if ($this->containerBuilder->hasParameter($key)) {
                $inputLine = $this->containerBuilder->getParameter($key);
                if (is_string($inputLine)) {
                    $inputLine = $this->formatParameter($inputLine);
                }
            } else {
                throw new ParameterNotFoundException($key);
            }
        }

        if (is_string($inputLine) && substr($inputLine, 0, 1) !== DIRECTORY_SEPARATOR) {
            $projectRoot = $this->containerBuilder->getParameter('project_root');
            $inputLine = $projectRoot . DIRECTORY_SEPARATOR . $inputLine;
        }

        return $inputLine;
    }

    /**
     * Рекурсивный поиск и подстановка параметров в строке
     *
     * @param $inputLine
     * @return mixed
     * @throws InvalidParameterException
     * @throws ParameterNotFoundException
     */
    protected function formatParameter($inputLine)
    {
        // Поиск и замена параметров внутри строки
        preg_match_all("/%([^%]+)%/", $inputLine, $outputArray);
        if (!empty($outputArray)) {
            foreach ($outputArray[1] as $key) {
                if ($this->containerBuilder->hasParameter($key)) {
                    $parameter = $this->containerBuilder->getParameter($key);
                    if (is_string($parameter)) {
                        $inputLine = str_replace('%' . $key . '%', $parameter, $inputLine);
                        $inputLine = $this->formatParameter($inputLine);
                    } else {
                        throw new InvalidParameterException($key);
                    }
                } else {
                    throw new ParameterNotFoundException($key);
                }
            }
        }
        return $inputLine;
    }

    /**
     * Инициализация DI для управления контейнерами из файла настроек
     *
     * @param OutputInterface $output
     */
    protected function loadContainerBuilder(OutputInterface $output = null)
    {
        $this->containerBuilder = new ContainerBuilder();

        $this->loadConfiguration();

        // Инициализация обьекта для вывода сообщений в консоли
        if ($output instanceof OutputInterface) {
            $this->containerBuilder->set('logger', $output);
        }
    }

    /**
     * Загрузка файла с настройками
     */
    protected function loadConfiguration()
    {
        if (file_exists(realpath(__DIR__ . '/../../../../../../vendor/autoload.php'))) {
            $projectRoot = realpath(__DIR__ . '/../../../../../../');
            $libraryRoot = dirname(dirname(dirname(__DIR__)));
            // Загрузка дефолтных конфигов
            $directory = $libraryRoot . DIRECTORY_SEPARATOR . 'project-template' . DIRECTORY_SEPARATOR . 'config';
            $this->loadConfigurationFile($directory);

            // Загрузка конфигов проекта
            $directory = $projectRoot . DIRECTORY_SEPARATOR . 'doc-generator' . DIRECTORY_SEPARATOR . 'config';
            $this->loadConfigurationFile($directory);
        } elseif (file_exists(realpath(__DIR__ . '/../../../vendor/autoload.php'))) {
            $projectRoot = dirname(dirname(dirname(__DIR__)));
            $libraryRoot = $projectRoot;
            // Загрузка дефолтных конфигов
            $directory = $projectRoot . DIRECTORY_SEPARATOR . 'project-template' . DIRECTORY_SEPARATOR . 'config';
            $this->loadConfigurationFile($directory);
        } else {
            $files = [];
            $files[] = realpath(__DIR__ . '/../../../../../../vendor/autoload.php');
            $files[] = realpath(__DIR__ . '/../../../vendor/autoload.php');
            throw new AutoloaderNotFoundException($files);
        }

        // Передаем в конфиги куорневую директорию проекта
        $this->containerBuilder->setParameter('project_root', $projectRoot);
        $this->containerBuilder->setParameter('library_root', $libraryRoot);
    }

    /**
     *
     * @param string $directory
     * @param bool   $required
     * @param string $prefix
     */
    protected function loadConfigurationFile($directory, $required = false, $prefix = 'services')
    {
        $locator = new FileLocator($directory);
        $loader = new YamlFileLoader($this->containerBuilder, $locator);

        // Подключение основного файла
        $file = $directory . DIRECTORY_SEPARATOR . $prefix . '.yml';

        if (file_exists($file)) {
            $loader->load($file);
        } elseif ($required) {
            throw new ConfigNotFoundException($file);
        }

        // Подключение дополнительного файла
        $file = $directory . DIRECTORY_SEPARATOR . $prefix . '.local.yml';
        if (file_exists($file)) {
            $loader->load($file);
        }
    }
}
