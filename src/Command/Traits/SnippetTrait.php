<?php

namespace moslibs\SwaggerMD\Command\Traits;

use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Сниппет для вставки текста в json по ключу
 * Используется перед сохранением json в файл
 */
trait SnippetTrait
{
    /**
     * Строитель контейнеров
     *
     * @var ContainerBuilder $containerBuilder
     */
    protected $containerBuilder;

    /**
     * Замена в файле подстрок данными из файла по маске
     *
     * @param $file
     * @param $text
     * @return mixed
     */
    protected function formatString($file, $text)
    {
        if (file_exists($file)) {
            $snippets = json_decode(file_get_contents($file), true);
            preg_match_all("/<!--\[%swg_snippet.(.+)\(\)%\]-->/", $text, $matches);
            if (!empty($matches)) {
                $keys = [];
                foreach ($matches[0] as $key => $match) {
                    $matchKey = $matches[1][$key];
                    if (!in_array($matchKey, $keys) && !empty($snippets[$matchKey])) {
                        $keys[] = $matchKey;
                        $replace = $snippets[$matchKey];
                        $text = str_replace($match, $replace, $text);
                    }
                }
            }
        }

        return $text;
    }
}
