<?php

namespace moslibs\SwaggerMD\Command;

use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Пересборка документации
 */
class BuildCommand extends AbstractCommand
{
    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->ignoreValidationErrors();

        $this->setName('swgmd-build')->setDefinition([
            $this->getBuildOption(),
            $this->getJsonFileOption(),
            $this->getDryRunOption(),
            $this->getScanOption(),
            $this->getExcludeOption(),
            $this->getDelimiterOption()
        ])->setDescription('ReCreate docs')->setHelp(<<<'EOF'
EOF
        );
    }

    /**
     * @inheritdoc
     */
    protected function executeBody(InputInterface $input, OutputInterface $output)
    {
        $build = $input->getOption('build');
        $jsonFile = $input->getOption('json-file');
        $scan = $input->getOption('scan');
        $dryRun = $input->getOption('dry-run');
        $exclude = $input->getOption('exclude');
        $delimiter = $input->getOption('delimiter');

        //Удалеие папки и файлов с документацией
        $args = [
            '--build'     => $build,
            '--json-file' => $jsonFile,
            '--dry-run'   => $dryRun
        ];

        $dryRunInput = new ArrayInput($args);
        $command = $this->getApplication()->find('swgmd-clean');
        $command->run($dryRunInput, $output);

        //Создание json файла на основе аннатаций
        $args = [
            '--build'     => $build,
            '--scan'      => $scan,
            '--exclude'   => $exclude,
            '--json-file' => $jsonFile,
            '--delimiter' => $delimiter,
            '--dry-run'   => $dryRun
        ];

        $dryRunInput = new ArrayInput($args);
        $command = $this->getApplication()->find('swgmd-scan');
        $command->run($dryRunInput, $output);

        //Создание документации на основе json файла
        $args = [
            '--build'     => $build,
            '--json-file' => $jsonFile,
            '--dry-run'   => $dryRun
        ];

        $dryRunInput = new ArrayInput($args);
        $command = $this->getApplication()->find('swgmd-md');
        $command->run($dryRunInput, $output);
    }
}
