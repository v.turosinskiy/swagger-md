<?php

namespace moslibs\SwaggerMD\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Эта команда удаляет директорию с документацией
 * и json файл
 */
class CleanCommand extends AbstractCommand
{
    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->ignoreValidationErrors();

        $this->setName('swgmd-clean')->setDefinition([
            $this->getBuildOption(),
            $this->getJsonFileOption(),
            $this->getDryRunOption(),
        ])->setDescription('Clear Markdown Docs directory')->setHelp(<<<'EOF'
EOF
        );
    }

    /**
     * @inheritdoc
     */
    protected function executeBody(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Load container builder...');
        $this->loadContainerBuilder($output);
        $output->writeln($this->line);

        $output->writeln('Load parameters...');
        //Проверка: Требуется ли создание файлов?
        $output->writeln('Dry-run:');
        $dryRun = $this->checkDryRunMode($input->getOption('dry-run'));
        $output->writeln('  - ' . (($dryRun) ? 'Enabled (Nothing will be changed)' : 'Disabled'));

        $filesystem = $this->containerBuilder->get('file_system');

        $dirs = $this->getParameterByMask($input->getOption('build'));
        $output->writeln('  - Target: ' . $dirs);
        $output->writeln($this->line);

        $output->writeln('Recursive deletion of files in a build directory...');
        if (!$dryRun) {
            $filesystem->remove($dirs);
        }
        $output->writeln('  - ' . $dirs . ' - Removed');
        $output->writeln($this->line);

        $output->writeln('Deletion json file...');
        $swgJsonFile = $this->getParameterByMask($input->getOption('json-file'));
        if (!$dryRun) {
            $filesystem->remove($swgJsonFile);
        }
        $output->writeln('  - ' . $swgJsonFile . ' - Removed');
    }
}
