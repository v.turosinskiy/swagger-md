<?php

namespace moslibs\SwaggerMD\Command;

use moslibs\SwaggerMD\Command\Traits\CommandTrait;
use moslibs\SwaggerMD\Command\Traits\MessagesTrait;
use moslibs\SwaggerMD\Command\Traits\OptionsTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Абстрактный класс для команд SwaggerMD
 */
abstract class AbstractCommand extends Command
{
    use CommandTrait, OptionsTrait, MessagesTrait;

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Собщение при выполнении команды
        $this->outputTitleMessage($output);

        // Выполнение команды
        $this->executeBody($input, $output);

        // Сообщение о завершении выполнения команды
        $this->outputFooterMessage($output);
    }

    abstract protected function executeBody(InputInterface $input, OutputInterface $output);
}
