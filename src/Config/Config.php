<?php

namespace moslibs\SwaggerMD\Config;

use moslibs\SwaggerMD\Helper\PageFactory;
use moslibs\SwaggerMD\Helper\Schema;
use moslibs\SwaggerMD\Helper\TableHelper;
use moslibs\SwaggerMD\Output\FileOutput;
use moslibs\SwaggerMD\Output\FileOutputInterface;
use Swagger\Annotations\Swagger;

/**
 * Конфиг генератора документации
 */
class Config
{
    /**
     * Документ в виде обьекта OpenAPI спецификации
     *
     * @var Swagger
     */
    private $swagger;

    /**
     * Шаблонизатор Twig
     *
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * Помощник для работы со схемой OpenAPI
     *
     * @var Schema
     */
    private $schema;

    /**
     * Помощник для генерации таблиц
     *
     * @var TableHelper
     */
    private $table;

    /**
     * Фабрика страниц
     *
     * @var PageFactory
     */
    private $pageFactory;

    /**
     * Обьект отвечающий за сохранение данных в файл
     *
     * @var FileOutput
     */
    private $fileOutput;

    public function __construct(
        Swagger $swagger,
        \Twig_Environment $twig,
        Schema $schema,
        TableHelper $table,
        PageFactory $pageFactory,
        FileOutput $fileOutput
    ) {
        $this->swagger = $swagger;
        $this->twig = $twig;
        $this->schema = $schema;
        $this->table = $table;
        $this->pageFactory = $pageFactory;
        $this->fileOutput = $fileOutput;
    }

    /**
     * Возвращает обьект на основе которого был создан swagger.json файл
     *
     * @return Swagger
     */
    public function getSwagger()
    {
        return $this->swagger;
    }

    /**
     * Возвращает шаблонизатор Twig
     *
     * @return \Twig_Environment
     */
    public function getTwig()
    {
        return $this->twig;
    }

    /**
     * Возвращает помощник, который содержит вспомогательные функции
     *
     * @return Schema
     */
    public function getSchemaHelper()
    {
        return $this->schema;
    }

    /**
     * Помощник для генерации markdown таблиц
     *
     * @return TableHelper
     */
    public function getTableHelper()
    {
        return $this->table;
    }

    /**
     * Возвращает фабрику страниц
     *
     * @return PageFactory
     */
    public function getPageFactory()
    {
        return $this->pageFactory;
    }

    /**
     * Возвращает обьект отвечающий за сохранение данных в файл
     *
     * @return FileOutputInterface
     */
    public function getFileOutput()
    {
        return $this->fileOutput;
    }
}