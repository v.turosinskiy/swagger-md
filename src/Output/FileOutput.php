<?php

namespace moslibs\SwaggerMD\Output;

use Symfony\Component\Filesystem\Filesystem;

/**
 * Класс отвечающий за создание файлов и дирректорий
 */
class FileOutput implements FileOutputInterface
{
    /**
     * Вспомогательный класс для создания файлов и дирректорий
     *
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * Конструктор
     *
     * FileOutput constructor.
     * @param Filesystem $filesystem
     */
    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    /**
     * @inheritdoc
     */
    public function save($file, $content)
    {
        $result = false;
        if (!empty($file) && !empty($content)) {
            $dir = dirname($file);
            $this->mkdir($dir);
            $this->filesystem->dumpFile($file, $content);
            $result = $this->filesystem->exists($file);
        }
        return $result;
    }

    /**
     * Рекурсивное создание дирректорий
     *
     * @param $dir - путь до дирректории для создания
     */
    protected function mkdir($dir)
    {
        $dir = dirname($dir);
        if (!$this->filesystem->exists($dir)) {
            $this->mkdir($dir);
        }
    }
}