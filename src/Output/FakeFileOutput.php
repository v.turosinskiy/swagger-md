<?php

namespace moslibs\SwaggerMD\Output;

/**
 * Заглушка для проверки работы команд без создания файлов
 */
class FakeFileOutput extends FileOutput
{
    /**
     * @inheritdoc
     */
    public function save($file, $content)
    {
        return true;
    }
}