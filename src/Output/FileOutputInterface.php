<?php

namespace moslibs\SwaggerMD\Output;

/**
 * Интерфейс отвечающий за создание markdown файлов
 */
interface FileOutputInterface
{
    /**
     * Сохраняет данные в файл
     *
     * @param string $file    - путь до файла
     * @param string $content - данные для записи в файл
     * @return bool
     */
    public function save($file, $content);
}