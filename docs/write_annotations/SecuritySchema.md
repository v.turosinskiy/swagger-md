# @SWG\SecuritySchema

## Описание
Класс [Swagger\Annotations\SecuritySchema](https://github.com/zircote/swagger-php/blob/master/src/Annotations/SecuritySchema.php)

Если указан, как свойство метода, то метод помечается как backend (Требует авторизации)
## Примеры
```php
 /*
  * @SWG\Get(
  *     ...
  *     security={{"api_key": {}}},
  *     ...
  * )
  */
```
    
[К Оглавлению](./../../README.md)