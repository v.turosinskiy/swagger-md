# @SWG\Operation

## Описание
Класс [Swagger\Annotations\Operation](https://github.com/zircote/swagger-php/blob/master/src/Annotations/Operation.php)

Является абстрактным, является родительским классом для:
- [@SWG\Get](./Get.md)
- [@SWG\Post](./Post.md)
- [@SWG\Options](./Options.md)
- [@SWG\Put](./Put.md)
- [@SWG\Delete](./Delete.md)
- [@SWG\Patch](./Patch.md)
- [@SWG\Head](./Head.md)

## Родительские аннотации

## Дочерние аннотации

## Свойства аннотации
- `path` (string) - путь вызова метода
- `method` (string) - тип метода
- `tags` (string) - тэг, используется для группировки методов
- `summary` (string) - Название метода
- `description` (string) - Описание метода
- `externalDocs` (string) - Ссылка на внешнюю документацию [@SWG\ExternalDocumentation](./ExternalDocumentation.md)
- `operationId` (string) - Идентификатор операции (не обязателен)
- `consumes` (array) - 
- `produces` (array) - Формат возвращаемых данных
- `parameters` [@SWG\Parameter[]](Parameter.md)
- `responses` (array) - Список возвращаемых ответов ([@SWG\Response](./Response.md))
- `schemes` (array) - Схема [@SWG\Schema](./Schema.md)
- `deprecated` (boolean) - пометка метода как устаревший
- `security` (array) - пометка метода как backend(требует авторизации)

## Примеры
```php
 * @SWG\Post(
 *     path="/api/{lang}/article",
 *     summary="Создание статьи",
 *     tags={"Статьи"},
 *     description="Метод используется для обновления поля комментария записи.",
 *     produces={"application/json"},
 *     @SWG\Parameter(
 *         ref="#/parameters/lang",
 *     ),
 *     @SWG\Parameter(
 *         ref="#/parameters/comment",
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="OK",
 *     ),
 *     security={{"api_key": {}}},
 * )
```

[К Оглавлению](./../../README.md)