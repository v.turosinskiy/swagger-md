# @SWG\Delete

## Описание
Класс [Swagger\Annotations\Delete](https://github.com/zircote/swagger-php/blob/master/src/Annotations/Delete.php)

## Родительские аннотации

## Дочерние аннотации

## Свойства аннотации
- `path` (string) 
- `method` (string) 
- `tags` (string)
- `summary` (string)
- `description` (string)
- `externalDocs` (string)
- `operationId` (string)
- `consumes` (array)
- `produces` (array)
- `parameters` [@SWG\Parameter[]](Parameter.md)
- `responses` (array)
- `schemes` (array)
- `deprecated` (boolean)
- `security` (array)

## Примеры
    
[К Оглавлению](./../../README.md)