# @SWG\Response

## Описание
Класс [Swagger\Annotations\Response](https://github.com/zircote/swagger-php/blob/master/src/Annotations/Response.php)

## Родительские аннотации

## Дочерние аннотации

## Свойства аннотации
- `name` - имя тэга(модели)
- `description` - краткое описание назначения тэга(модели)
- `externalDocs` - ссылка на подробную документацию с описанием тэга(модели)

## Примеры

1. Вернуть модель

```php
 *     @SWG\Response(
 *         response=200,
 *         @SWG\Schema(
 *             ref="#/definitions/Molding"
 *         )
 *     ),
```

2. Вернуть свойство модели

```php
 *     @SWG\Response(
 *         response=200,
 *         @SWG\Schema(
 *             ref="#/definitions/Molding/properties/type"
 *         )
 *     ),
```

3. Вернуть массив моделей
```php
 *     @SWG\Response(
 *         response=200,
 *         @SWG\Schema(
 *             property="items",
 *             type="array",
 *             @SWG\Items(
 *                 ref="#/definitions/Molding"              
 *             )
 *         )
 *     ),
```

4. Вернуть массив моделей содержащие определенные свойства

```php
 *     @SWG\Response(
 *         response=200,
 *         @SWG\Schema(
 *             type="array",
 *             @SWG\Items(
 *                 @SWG\Property(
 *                    property="model_id",
 *                    ref="#/definitions/Molding/properties/model_id"
 *                 ),
 *                 @SWG\Property(
 *                    property="type",
 *                    ref="#/definitions/Molding/properties/type"
 *                 )
 *             )
 *         )
 *     ),
```

[К Оглавлению](./../../README.md)