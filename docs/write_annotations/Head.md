# @SWG\Head

## Описание
Класс [Swagger\Annotations\Head](https://github.com/zircote/swagger-php/blob/master/src/Annotations/Head.php)

Описывает структуру Head запросов. Наследник класса [@SWG\Operation](./Operation.md)

[К Оглавлению](./../../README.md)