# @SWG\Items

## Описание
Класс [Swagger\Annotations\Items](https://github.com/zircote/swagger-php/blob/master/src/Annotations/Items.php)

## Родительские аннотации
1. [@SWG\Parameter](Parameter.md)
2. [@SWG\Property](Property.md)
3. [@SWG\Schema](Schema.md)
4. [@SWG\Definition](Definition.md)
5. [@SWG\Items](Items.md)
6. [@SWG\Header](Header.md)
## Дочерние аннотации

## Свойства аннотации

## Примеры

[К Оглавлению](./../../README.md)