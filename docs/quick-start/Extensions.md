# Работа с расширениями

Оглавление:  
1. [Что такое расширения?](#extensions)
2. [Создание расширений](#create-extensions)
3. [Подключение/отключение расширения](#add-extensions)

<a name="extensions"></a>
## Дополнительные атрибуты
Расширения это набор классов, которые позволяют модифицировать данные до момента когда эти данные будут переданы в шаблон.  

Стандартные расширения расположены в дирректории [Extension](./../../src/PageService/Extension).  

Список стандартных расширений:
- [ExampleParametersExtension](./../../src/PageService/Extension/ExampleParametersExtension.php)
- [ExampleResponseExtension](./../../src/PageService/Extension/ExampleResponseExtension.php)
- [ExpandParameterExtension](./../../src/PageService/Extension/ExpandParameterExtension.php)
- [FilterParameterExtension](./../../src/PageService/Extension/FilterParameterExtension.php)
- [NullAbleFieldExtension](./../../src/PageService/Extension/NullableFieldExtension.php)
- [OptionalFieldExtension](./../../src/PageService/Extension/OptionalFieldExtension.php)
- [PostParametersExtension](./../../src/PageService/Extension/PostParametersExtension.php)
- [SortParameterExtension](./../../src/PageService/Extension/SortParameterExtension.php)

<a name="create-extensions"></a>
## Создание расширений

Для создания расширения необходимо:
1. Создать класс наследующий интерфейс [ExtensionInterface](../../src/PageService/Extension/ExtensionInterface.php)  
2. Добавить описание расширения в файл настроек [services.yml](./../../project-template/config/services.local.yml)    
    На примере `ExampleResponseExtension`:  
    ```yaml
    parameters:
        ...
    services:
        ...
        ps_ext_example_response:
            class: SwaggerMD\PageService\Extension\ExampleResponseExtension
            arguments: ['@schema']
        ...
    ```
<a name="add-extensions"></a>
## Подключение/отключение расширения

Для подключения/отключения используется файл [services.yml](./../../project-template/config/services.local.yml)

Рассмотрим на примере `ExampleResponseExtension`.  

Подключение расширения:  
```yaml
parameters:
    ...
services:
    ...
    ps_action:
        class: SwaggerMD\PageService\ActionDetailPageService
        arguments: ['@gen_config']
        calls:
            - [setTemplateName, ['%ps.action_detail.tpl%']]
            - [registerExtension, ['@ps_ext_example_response']]
            ...
    ...
```   

Отключение расширения:    
```yaml
parameters:
    ...
services:
    # ...
    ps_action:
        class: SwaggerMD\PageService\ActionDetailPageService
        arguments: ['@gen_config']
        calls:
            - [setTemplateName, ['%ps.action_detail.tpl%']]
            #- [registerExtension, ['@ps_ext_example_response']]
            ...
    ...
```   

[К Оглавлению](./../../README.md)