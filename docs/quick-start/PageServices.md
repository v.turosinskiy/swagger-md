# Работа с "Сервисами страниц"

Оглавление:  
1. [Что такое "Сервис страниц"?](#about)
2. [Создание нового "Сервиса страниц"](#create)
3. [Управление "Сервисами страниц"](#manage)
4. [Модификация "Сервиса страниц"](#modify)
5. [Изменение шаблона "Сервиса страниц"](#templates)

<a name="about"></a>
## Что такое "Сервис страниц"?
"Сервис страниц" это класс, который отвечает за формирование отдельного типа страниц.    

| Тип страницы              | Сервис страниц               |
|:---                       |------------------------------|
| Главная страница          | [TableOfContentsPageService] |
| Списком методов           | [ActionsPageService]         |
| Список моделей            | [ModelsPageService]          |
| Список свойств            | [PropertiesPageService]      |
| Список параметров         | [ParametersPageService]      |
| Детальная страница метода | [ActionDetailPageService]    |
| Детальная страница модели | [ModelDetailPageService]     |

<a name="create"></a>
## Создание нового "Сервиса страниц"

Для создания нового "Сервиса страниц" необходимо:
1. Создать класс осуществляющий интерфейс [PageServiceInterface]
2. Добавить описание инициализации сервиса в файл с настройками [services.yml]

Пример описания "Сервиса страниц" для главной страницы:
```yaml
services:
    ...
    ps_table_of_contents: # строковый идентификатор сервиса
        class: SwaggerMD\PageService\TableOfContentsPageService # полное название класса
        arguments: ['@gen_config'] # список аргументов, которые будут добавлены в конструктор при инициализации
        calls:
            - [setTemplateName, ['%ps.table_of_contents.tpl%']] # Путь до шаблона, берется из параметров
    ...
```

<a name="manage"></a>
## Управление "Сервисами страниц"
Включение и отключение сервисов определяется настройками инициализации класса [Generator] в файле [services.yml]

Рассмотрим включение и отключение сервиса на примере главной старницы.

Включение:  
```yaml
parameters:
    ...
services:
    ...
    generator:
        class: SwaggerMD\Generator
        arguments: ['@gen_config']
        calls:
            ...
            - [addPageService, ['@ps_table_of_contents']]
            ...
    ...
```

Отключение:
```yaml
parameters:
    ...
services:
    ...
    generator:
        class: SwaggerMD\Generator
        arguments: ['@gen_config']
        calls:
            ...
            #- [addPageService, ['@ps_table_of_contents']]
            ...
    ...
```

<a name="modify"></a>
## Модификация "Сервиса страниц"
Для изменения уже существующего сервиса достаточно скопировать уже существующий сервис и заменить подключение уже существующего сервиса на свой собственный.

<a name="templates"></a>
## Изменение шаблона "Сервиса страниц"
К каждому сервису привязан один шаблон страницы.   
Способы переопределить шаблон:
1. Переопределить текущее значение параметра в значениях `parameters`
2. Переопределить ключ параметра в настройках `calls`
3. Создать новый параметр и переопределить значение в настройках `calls`

Пример настроек для определения шаблонов у "Сервисов страниц":  
```yaml
parameters:
    ...
    # Относительный путь к шаблонам страниц
    ps.table_of_contents.tpl: '%pages.directory%/table_of_contents/page.twig'
    ...
services:
    ...
    ps_table_of_contents:
        class: SwaggerMD\PageService\TableOfContentsPageService
        arguments: ['@gen_config']
        calls:
            - [setTemplateName, ['%ps.table_of_contents.tpl%']]
    ...
```
[К Оглавлению](./../../README.md)

[PageServiceInterface]: ../../src/PageService/PageServiceInterface.php
[TableOfContentsPageService]: ../../src/PageService/TableOfContentsPageService.php
[ActionsPageService]: ../../src/PageService/ActionsPageService.php
[ModelsPageService]: ../../src/PageService/ModelsPageService.php
[PropertiesPageService]: ../../src/PageService/PropertiesPageService.php
[ParametersPageService]: ../../src/PageService/ParametersPageService.php
[ActionDetailPageService]: ../../src/PageService/ActionDetailPageService.php
[ModelDetailPageService]: ../../src/PageService/ModelDetailPageService.php
[services.yml]: ../../project-template/config/services.yml
[services.local.yml]: ../../project-template/config/services.local.yml
[Generator]: ../../src/Generator.php