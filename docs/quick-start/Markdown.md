# Особенности генерации Markdown документации

Оглавление:  
1. [Дополнительные атрибуты](#additional)
2. [Использование снипетов](#replace-text)
2. [Использование ссылок](#links)
<a name="additional"></a>
## Дополнительные атрибуты
Markdown документация формируется на основе swagger.json файла.

Open API спецификация содержит ограниченный набор атрибутов для описания сервисов. Для их расширения используется атрибут `x-somefield`, который позволяет расширениям хранить необходимые данные в этих полях.

Список параметров на которые стоит обратить внимание:
1. @SWG\Property
  - `nullable` - свойство может принимать значение null
  - `filtrable` - свойство подлежит фильтрации
  - `sortable` - свойство подлежит сортировке
  - `deprecated` - свойство помечено как Deprecated и не рекомендуется для использования
  - `post` - описание свойства при использовании его в качестве `POST` параметра
2. @SWG\Definition
  - `deprecated` - Модель помечена как Deprecated и не рекомендуется для использования
  - `namespace` - вложенность моделей на странице списка моделей
3. @SWG\Action
  - `filter` - прямое указание на список свойств, которые подлежат фильтрации
  - `sort` - прямое указание на список свойств, которые подлежат сортировке

<a name="replace-text"></a>
## Вставка текста
Некоторые тексты повторяются в нескольких местах. Такие тексты переносятся в отдельный файл [replace.json](../../project-template/snippets/replace.json).    
Пример файла snippet.json:
```json
{
  "text_for_replace": "Hello world",
  "another_text_for_replace": "Goodbye world!"
}
```

Для автоподстановки текста необходимо обернуть `идентификатор` из файла в html комментарии по формату:    
`<!--[%swg_snippet.another_text_for_replace()%]-->`

Пример добавления текстового блока в аннотации:
```php
<?php
/**
 * @SWG\Get(
 *     ...
 *     description="<!--[%swg_snippet.text_for_replace()%]-->",
 *     ...
 */
```
После создания `swagger.json` файл будет содержать следующее:
```
  ...
  "description": "Hello world",
  ...
```

<a name="links"></a>
## Использование ссылок
Для того, чтобы избежать дублирования параметров и свойств рекомендуется использовать при описании ссылки.

По умолчанию все свойства хранятся в файле `/annotations/properties.php`
Все параметры храняться в файле `/annotations/parameters.php`

Примеры использования ссылок:
1. Ссылка на параметры
    ```php
    * @SWG\Parameter(
    *     name="tags",
    *     ref="#/parameters/tags"
    * ),
    ```
2. Ссылка на параметры
    ```php
    * @SWG\Property(
    *     property="lang",
    *     ref="#/definitions/CommonProperties/properties/lang"
    * ),
    ```
3. Ссылка на модели
    ```php
    * @SWG\Definition(definition="AttachPublic", title="Публичные приложенные файлы", type="object", @SWG\Xml(name="AttachPublic"),
    *     x={"namespace"="Приложенные файлы"},
    *     ref="$/definitions/Attach"
    * )
    ```
    Необходимо обратить внимание ссылка формируется чере знак `$`, а не '#'.

4. Ссылка на свойства модели
    ```php
    * @SWG\Property(
    *     property="id",
    *     ref="#/definitions/Attach/properties/id"
    * ),
    ```
5. Привязка модели в виде свойства к другой модели или в схеме как возвращаемое значение
    ```php
    * @SWG\Property(
    *     property="attach",
    *     ref="#/definitions/Attach"
    * ),
    ```

[К Оглавлению](./../../README.md)