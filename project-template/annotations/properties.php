<?php

/**
 *
 * @SWG\Definition(definition="CommonTypes",
 *     @SWG\Property(
 *         property="boolean",
 *         description="Логический",
 *         type="integer",
 *         enum={0, 1},
 *         example=1,
 *     ),
 *     @SWG\Property(
 *         property="dateDefault",
 *         description="Дата по формату: yyyy-mm-dd",
 *         type="string",
 *         format="date",
 *         example="2017-10-24",
 *     ),
 *     @SWG\Property(
 *         property="datetimeDefault",
 *         description="Дата и время по формату: yyyy-mm-dd HH:ii:ss",
 *         type="string",
 *         format="dateTime",
 *         example="2018-01-06 13:17:40",
 *     ),
 *     @SWG\Property(
 *         property="datetimeMaterial",
 *         description="Дата и время по формату: dd.mm.yyyy HH:ii:ss",
 *         type="string",
 *         example="27.05.2016 10:00:00",
 *     ),
 *     @SWG\Property(
 *         property="timestamp",
 *         description="Поле типа timestamp",
 *         type="integer",
 *         example=1464324480,
 *     ),
 *     @SWG\Property(
 *         property="timestampNegative",
 *         description="Отрицательный timestamp",
 *         type="integer",
 *         example=-62169993079,
 *     ),
 * ),
 *
 *
 *
 *
 *
 * @SWG\Definition(
 *     definition="CommonProperties",
 * ),
 */
